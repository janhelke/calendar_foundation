<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

/**
 * Register icons
 */
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

$iconRegistry->registerIcon(
    'tx-calendar-entry',
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:calendar_foundation/Resources/Public/Icons/tx_calendar_entry.svg']
);
$iconRegistry->registerIcon(
    'tx-calendar-entry-deviation',
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:calendar_foundation/Resources/Public/Icons/tx_calendar_entry_deviation.svg']
);
$iconRegistry->registerIcon(
    'tx-calendar-entry-exception',
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:calendar_foundation/Resources/Public/Icons/tx_calendar_entry_exception.svg']
);
$iconRegistry->registerIcon(
    'tx-calendar-calendar',
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:calendar_foundation/Resources/Public/Icons/tx_calendar_calendar.svg']
);
