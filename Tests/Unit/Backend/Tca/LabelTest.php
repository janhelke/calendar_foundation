<?php
declare(strict_types=1);

namespace JanHelke\CalendarFoundation\Tests\Functional\Backend\Tca;

use JanHelke\CalendarFoundation\Backend\Tca\Label;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Cache\Frontend\FrontendInterface;
use TYPO3\CMS\Core\Localization\LanguageService;
use TYPO3\CMS\Core\Localization\LanguageStore;
use TYPO3\CMS\Core\Localization\Locales;
use TYPO3\CMS\Core\Localization\LocalizationFactory;
use TYPO3\CMS\Core\Package\PackageManager;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Label test
 */
class LabelTest extends UnitTestCase
{
    use ProphecyTrait;

    /**
     * @return array
     */
    public function getRecurrenceLabelDataProvider(): array
    {
        return [
            'default' => [
                'parameters' => [
                    'table' => 'tx_calendar_recurrence',
                    'row' => [],
                    'title' => 'Foo'
                ],
                'expectedLabel' => 'Foo'
            ],
            'daily_every_day' => [
                'parameters' => [
                    'table' => 'tx_calendar_recurrence',
                    'row' => [
                        'frequency' => [
                            0 => 1
                        ],
                        'interval' => 1
                    ]
                ],
                'expectedLabel' => 'Daily, every day'
            ],
            'daily_every_second_day' => [
                'parameters' => [
                    'table' => 'tx_calendar_recurrence',
                    'row' => [
                        'frequency' => [
                            0 => 1
                        ],
                        'interval' => 2
                    ]
                ],
                'expectedLabel' => 'Daily, every 2 days'
            ],
            'weekly_no_day' => [
                'parameters' => [
                    'table' => 'tx_calendar_recurrence',
                    'row' => [
                        'frequency' => [
                            0 => 2
                        ],
                        'interval' => 1
                    ]
                ],
                'expectedLabel' => 'Weekly, invalid selection'
            ],
            'weekly_every_monday' => [
                'parameters' => [
                    'table' => 'tx_calendar_recurrence',
                    'row' => [
                        'frequency' => [
                            0 => 2
                        ],
                        'by_day_of_week' => 1,
                        'interval' => 1,
                    ],
                    'options' => [
                        'by_day_of_week' => [
                            [
                                'Mo',
                                ''
                            ]
                        ],
                    ]
                ],
                'expectedLabel' => 'Weekly, every Mo'
            ],
            'weekly_every_second_monday' => [
                'parameters' => [
                    'table' => 'tx_calendar_recurrence',
                    'row' => [
                        'frequency' => [
                            0 => 2
                        ],
                        'by_day_of_week' => 1,
                        'interval' => 2,
                    ],
                    'options' => [
                        'by_day_of_week' => [
                            [
                                'Mo',
                                ''
                            ]
                        ],
                    ]
                ],
                'expectedLabel' => 'Weekly, every 2 Mo'
            ],
            'weekly_every_third_monday_wednesday' => [
                'parameters' => [
                    'table' => 'tx_calendar_recurrence',
                    'row' => [
                        'frequency' => [
                            0 => 2
                        ],
                        'by_day_of_week' => 5,
                        'interval' => 3,
                    ],
                    'options' => [
                        'by_day_of_week' => [
                            [
                                'Mo',
                                ''
                            ],
                            [
                                'Tu',
                                ''
                            ],
                            [
                                'We',
                                ''
                            ]
                        ],
                    ]
                ],
                'expectedLabel' => 'Weekly, every 3 Mo, We'
            ],
            'weekly_every_fourth_week_six_days' => [
                'parameters' => [
                    'table' => 'tx_calendar_recurrence',
                    'row' => [
                        'frequency' => [
                            0 => 2
                        ],
                        'by_day_of_week' => 63,
                        'interval' => 4,
                    ],
                    'options' => [
                        'by_day_of_week' => [
                            [
                                'Mo',
                                ''
                            ],
                            [
                                'Tu',
                                ''
                            ],
                            [
                                'We',
                                ''
                            ],
                            [
                                'Th',
                                ''
                            ],
                            [
                                'Fr',
                                ''
                            ],
                            [
                                'Sa',
                                ''
                            ],
                            [
                                'Su',
                                ''
                            ]
                        ],
                    ]
                ],
                'expectedLabel' => 'Weekly, every 4 Mo, Tu, We, Th, Fr, Sa'
            ],
            'monthly_invalid_selection' => [
                'parameters' => [
                    'row' => [
                        'frequency' => [
                            0 => 3
                        ],
                    ],
                    'table' => 'tx_calendar_recurrence',
                ],
                'expectedLabel' => 'Monthly, invalid selection'
            ],
            'monthly_every_first_day' => [
                'parameters' => [
                    'table' => 'tx_calendar_recurrence',
                    'row' => [
                        'frequency' => [
                            0 => 3
                        ],
                        'by_day_of_month' => 1,
                        'interval' => 1
                    ],
                    'options' => [
                        'by_day_of_month' => [
                            [
                                '1',
                                ''
                            ]
                        ],
                    ]
                ],
                'expectedLabel' => 'Monthly, every 1'
            ],
            'monthly_every_second_ten_days' => [
                'parameters' => [
                    'table' => 'tx_calendar_recurrence',
                    'row' => [
                        'frequency' => [
                            0 => 3
                        ],
                        'by_day_of_month' => 1023,
                        'interval' => 2
                    ],
                    'options' => [
                        'by_day_of_month' => [
                            [
                                '1',
                                ''
                            ],
                            [
                                '2',
                                ''
                            ],
                            [
                                '3',
                                ''
                            ],
                            [
                                '4',
                                ''
                            ],
                            [
                                '5',
                                ''
                            ],
                            [
                                '6',
                                ''
                            ],
                            [
                                '7',
                                ''
                            ],
                            [
                                '8',
                                ''
                            ],
                            [
                                '9',
                                ''
                            ],
                            [
                                '10',
                                ''
                            ],
                            [
                                '11',
                                ''
                            ],
                        ],
                    ]
                ],
                'expectedLabel' => 'Monthly, every 2 1, 2, 3, 4, 5, 6, 7, 8, 9, 10'
            ],
            'monthly_every_second_ten_days_minus_one' => [
                'parameters' => [
                    'table' => 'tx_calendar_recurrence',
                    'row' => [
                        'frequency' => [
                            0 => 3
                        ],
                        'by_day_of_month' => 991,
                        'interval' => 2
                    ],
                    'options' => [
                        'by_day_of_month' => [
                            [
                                '1',
                                ''
                            ],
                            [
                                '2',
                                ''
                            ],
                            [
                                '3',
                                ''
                            ],
                            [
                                '4',
                                ''
                            ],
                            [
                                '5',
                                ''
                            ],
                            [
                                '6',
                                ''
                            ],
                            [
                                '7',
                                ''
                            ],
                            [
                                '8',
                                ''
                            ],
                            [
                                '9',
                                ''
                            ],
                            [
                                '10',
                                ''
                            ],
                            [
                                '11',
                                ''
                            ],
                        ],
                    ]
                ],
                'expectedLabel' => 'Monthly, every 2 1, 2, 3, 4, 5, 7, 8, 9, 10'
            ],
            'monthly_every_last_friday' => [
                'parameters' => [
                    'table' => 'tx_calendar_recurrence',
                    'row' => [
                        'frequency' => [
                            0 => 3
                        ],
                        'by_day_index' => -1,
                        'by_day_day' => 6
                    ],
                    'options' => [
                        'by_day_of_week' => [
                            [
                                'Friday',
                                6
                            ]
                        ],
                        'by_day_index' => [
                            [
                                'Last',
                                -1
                            ],
                        ],
                    ]
                ],
                'expectedLabel' => 'Monthly, every Last Friday'
            ],
            'yearly' => [
                'parameters' => [
                    'table' => 'tx_calendar_recurrence',
                    'row' => [
                        'frequency' => [
                            0 => 4
                        ],
                        'interval' => 1
                    ]
                ],
                'expectedLabel' => 'Yearly'
            ],
            'yearly_every_second' => [
                'parameters' => [
                    'table' => 'tx_calendar_recurrence',
                    'row' => [
                        'frequency' => [
                            0 => 4
                        ],
                        'interval' => 2
                    ]
                ],
                'expectedLabel' => 'Yearly, every 2'
            ],
            'daily_every_day with exception' => [
                'parameters' => [
                    'table' => 'tx_calendar_recurrence',
                    'row' => [
                        'frequency' => [
                            0 => 1
                        ],
                        'interval' => 1,
                        'exception' => [
                            'foo' => 'bar'
                        ]
                    ]
                ],
                'expectedLabel' => 'Daily, every day (with exception)'
            ],
            'daily_every_day with deviation' => [
                'parameters' => [
                    'table' => 'tx_calendar_recurrence',
                    'row' => [
                        'frequency' => [
                            0 => 1
                        ],
                        'interval' => 1,
                        'deviation' => [
                            'foo' => 'bar'
                        ]
                    ]
                ],
                'expectedLabel' => 'Daily, every day (with deviation)'
            ],
            'daily_every_day with exception and deviation' => [
                'parameters' => [
                    'table' => 'tx_calendar_recurrence',
                    'row' => [
                        'frequency' => [
                            0 => 1
                        ],
                        'interval' => 1,
                        'exception' => [
                            'foo' => 'bar'
                        ],
                        'deviation' => [
                            'foo' => 'bar'
                        ]
                    ]
                ],
                'expectedLabel' => 'Daily, every day (with exception and deviation)'
            ],
            'blank' => [
                'parameters' => [
                    'table' => 'tx_calendar_recurrence',
                    'row' => []
                ],
                'expectedLabel' => ''
            ]
        ];
    }

    /**
     * @dataProvider getRecurrenceLabelDataProvider
     * @param array $parameters
     * @param string $expectedLabel
     */
    public function testGetRecurrenceLabel(array $parameters, string $expectedLabel): void
    {
        $this->resetSingletonInstances = true;

        $typo3Version = VersionNumberUtility::convertVersionNumberToInteger(VersionNumberUtility::getCurrentTypo3Version() ?? 'TYPO3_version');
        if ($typo3Version < 11_003_000) {
            $languageService = $this->prophesize(LanguageService::class);
            $languageService->sL(Argument::cetera())->willReturnArgument(0);
            $GLOBALS['LANG'] = $languageService->reveal();
        } else {
            /** @var PackageManager $packageManagerProphecy */
            $packageManagerProphecy = $this->prophesize(PackageManager::class);
            /** @var FrontendInterface $cacheFrontendProphecy */
            $cacheFrontendProphecy = $this->prophesize(FrontendInterface::class);
            $cacheFrontendProphecy->get(Argument::cetera())->willReturn(false);
            $cacheFrontendProphecy->set(Argument::cetera())->willReturn(null);
            /** @var CacheManager $cacheManagerProphecy */
            $cacheManagerProphecy = $this->prophesize(CacheManager::class);
            $cacheManagerProphecy->getCache('pages')->willReturn($cacheFrontendProphecy->reveal());
            $cacheManagerProphecy->getCache('l10n')->willReturn($cacheFrontendProphecy->reveal());
            GeneralUtility::setSingletonInstance(CacheManager::class, $cacheManagerProphecy->reveal());
            $languageService = new LanguageService(
                new Locales(),
                new LocalizationFactory(
                    new LanguageStore($packageManagerProphecy->reveal()),
                    $cacheManagerProphecy->reveal()
                ),
                $cacheFrontendProphecy->reveal()
            );

            $GLOBALS['LANG'] = $languageService;
        }

        (new Label())->getRecurrenceLabel($parameters);

        self::assertEquals($expectedLabel, $parameters['title'] ?? '');
    }
}
