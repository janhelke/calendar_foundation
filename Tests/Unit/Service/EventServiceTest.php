<?php
declare(strict_types=1);

namespace JanHelke\CalendarFoundation\Tests\Unit\Service;

use DateTime;
use Exception;
use JanHelke\CalendarFoundation\Service\EventService;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Event service test
 */
class EventServiceTest extends UnitTestCase
{

    /**
     * @return array
     */
    public function calculateEventRowsDataProvider(): array
    {
        $calendarEntryDatabaseRow = [
            'uid' => 23,
            'start' => '2019-06-26 12:13:00',
            'end' => '2019-06-26 17:47:00',
            'all_day' => 0,
        ];

        $calendarAllDayEntryDatabaseRow = [
            'uid' => 23,
            'start' => '2019-06-26 12:13:00',
            'end' => '2019-06-26 17:47:00',
            'all_day' => 1,
        ];

        $calendarMoreDayEntryDatabaseRow = [
            'uid' => 23,
            'start' => '2019-06-26 12:13:00',
            'end' => '2019-07-02 17:47:00',
            'all_day' => 0,
        ];

        $calendarMoreDayAllDayEntryDatabaseRow = [
            'uid' => 23,
            'start' => '2019-06-26 12:13:00',
            'end' => '2019-07-02 17:47:00',
            'all_day' => 1,
        ];

        return [
            'normal_entry_no_recurrences' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'all_day_entry_no_recurrences' => [
                'calendarEntry' => $calendarAllDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 00:00:00',
                        'end' => '2019-06-26 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'more_day_entry_no_recurrences' => [
                'calendarEntry' => $calendarMoreDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'more_day_all_day_entry_no_recurrences' => [
                'calendarEntry' => $calendarMoreDayAllDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 00:00:00',
                        'end' => '2019-07-02 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],

            /**
             * Daily test cases
             */
            'normal_entry_1_daily_recurrence_every_day' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 1
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-27 12:13:00',
                        'end' => '2019-06-27 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal_entry_1_daily_recurrence_every_five_days' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 5,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 1
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-01 12:13:00',
                        'end' => '2019-07-01 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal_entry_5_daily_recurrence_every_three_days' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 3,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 5
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-29 12:13:00',
                        'end' => '2019-06-29 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-05 12:13:00',
                        'end' => '2019-07-05 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-08 12:13:00',
                        'end' => '2019-07-08 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-11 12:13:00',
                        'end' => '2019-07-11 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal_entry_daily_recurrence_every_six_days_with_end_date' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 6,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-11 00:00:00',
                    'until_recurrence_amount' => 0
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-08 12:13:00',
                        'end' => '2019-07-08 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal_entry_daily_recurrence_every_six_days_with_end_date_and_amount' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 6,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-11 00:00:00',
                    'until_recurrence_amount' => 2
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-08 12:13:00',
                        'end' => '2019-07-08 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal_entry_daily_recurrence_every_six_days_with_end_date_and_greater_amount' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 6,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-11 00:00:00',
                    'until_recurrence_amount' => 5
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-08 12:13:00',
                        'end' => '2019-07-08 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal_entry_daily_recurrence_every_six_days_with_end_date_and_smaller_amount' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 6,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-11 00:00:00',
                    'until_recurrence_amount' => 1
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],

            'all_day_entry_1_daily_recurrence_every_day' => [
                'calendarEntry' => $calendarAllDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 1
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 00:00:00',
                        'end' => '2019-06-26 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-27 00:00:00',
                        'end' => '2019-06-27 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'all_day_entry_1_daily_recurrence_every_five_days' => [
                'calendarEntry' => $calendarAllDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 5,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 1
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 00:00:00',
                        'end' => '2019-06-26 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-01 00:00:00',
                        'end' => '2019-07-01 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'all_day_entry_5_daily_recurrence_every_three_days' => [
                'calendarEntry' => $calendarAllDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 3,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 5
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 00:00:00',
                        'end' => '2019-06-26 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-29 00:00:00',
                        'end' => '2019-06-29 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 00:00:00',
                        'end' => '2019-07-02 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-05 00:00:00',
                        'end' => '2019-07-05 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-08 00:00:00',
                        'end' => '2019-07-08 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-11 00:00:00',
                        'end' => '2019-07-11 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'all_day_entry_daily_recurrence_every_six_days_with_end_date' => [
                'calendarEntry' => $calendarAllDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 6,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-11 00:00:00',
                    'until_recurrence_amount' => 0
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 00:00:00',
                        'end' => '2019-06-26 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 00:00:00',
                        'end' => '2019-07-02 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-08 00:00:00',
                        'end' => '2019-07-08 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'all_day_entry_daily_recurrence_every_six_days_with_end_date_and_amount' => [
                'calendarEntry' => $calendarAllDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 6,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-11 00:00:00',
                    'until_recurrence_amount' => 2
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 00:00:00',
                        'end' => '2019-06-26 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 00:00:00',
                        'end' => '2019-07-02 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-08 00:00:00',
                        'end' => '2019-07-08 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'all_day_entry_daily_recurrence_every_six_days_with_end_date_and_greater_amount' => [
                'calendarEntry' => $calendarAllDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 6,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-11 00:00:00',
                    'until_recurrence_amount' => 5
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 00:00:00',
                        'end' => '2019-06-26 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 00:00:00',
                        'end' => '2019-07-02 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-08 00:00:00',
                        'end' => '2019-07-08 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'all_day_entry_daily_recurrence_every_six_days_with_end_date_and_smaller_amount' => [
                'calendarEntry' => $calendarAllDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 6,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-11 00:00:00',
                    'until_recurrence_amount' => 1
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 00:00:00',
                        'end' => '2019-06-26 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 00:00:00',
                        'end' => '2019-07-02 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],

            'more_day_entry_1_daily_recurrence_every_day' => [
                'calendarEntry' => $calendarMoreDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 1
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-27 12:13:00',
                        'end' => '2019-07-03 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'more_day_entry_1_daily_recurrence_every_five_days' => [
                'calendarEntry' => $calendarMoreDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 5,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 1
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-01 12:13:00',
                        'end' => '2019-07-07 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'more_day_entry_5_daily_recurrence_every_three_days' => [
                'calendarEntry' => $calendarMoreDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 3,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 5
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-29 12:13:00',
                        'end' => '2019-07-05 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-08 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-05 12:13:00',
                        'end' => '2019-07-11 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-08 12:13:00',
                        'end' => '2019-07-14 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-11 12:13:00',
                        'end' => '2019-07-17 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'more_day_entry_daily_recurrence_every_six_days_with_end_date' => [
                'calendarEntry' => $calendarMoreDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 6,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-11 00:00:00',
                    'until_recurrence_amount' => 0
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-08 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-08 12:13:00',
                        'end' => '2019-07-14 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'more_day_entry_daily_recurrence_every_six_days_with_end_date_and_amount' => [
                'calendarEntry' => $calendarMoreDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 6,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-11 00:00:00',
                    'until_recurrence_amount' => 2
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-08 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-08 12:13:00',
                        'end' => '2019-07-14 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'more_day_entry_daily_recurrence_every_six_days_with_end_date_and_greater_amount' => [
                'calendarEntry' => $calendarMoreDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 6,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-11 00:00:00',
                    'until_recurrence_amount' => 5
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-08 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-08 12:13:00',
                        'end' => '2019-07-14 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'more_day_entry_daily_recurrence_every_six_days_with_end_date_and_smaller_amount' => [
                'calendarEntry' => $calendarMoreDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 6,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-11 00:00:00',
                    'until_recurrence_amount' => 1
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-08 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],

            'more_day_all_day_entry_1_daily_recurrence_every_day' => [
                'calendarEntry' => $calendarMoreDayAllDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 1
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 00:00:00',
                        'end' => '2019-07-02 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-27 00:00:00',
                        'end' => '2019-07-03 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'more_day_all_day_entry_1_daily_recurrence_every_five_days' => [
                'calendarEntry' => $calendarMoreDayAllDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 5,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 1
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 00:00:00',
                        'end' => '2019-07-02 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-01 00:00:00',
                        'end' => '2019-07-07 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'more_day_all_day_entry_5_daily_recurrence_every_three_days' => [
                'calendarEntry' => $calendarMoreDayAllDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 3,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 5
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 00:00:00',
                        'end' => '2019-07-02 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-29 00:00:00',
                        'end' => '2019-07-05 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 00:00:00',
                        'end' => '2019-07-08 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-05 00:00:00',
                        'end' => '2019-07-11 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-08 00:00:00',
                        'end' => '2019-07-14 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-11 00:00:00',
                        'end' => '2019-07-17 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'more_day_all_day_entry_daily_recurrence_every_six_days_with_end_date' => [
                'calendarEntry' => $calendarMoreDayAllDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 6,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-11 00:00:00',
                    'until_recurrence_amount' => 0
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 00:00:00',
                        'end' => '2019-07-02 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 00:00:00',
                        'end' => '2019-07-08 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-08 00:00:00',
                        'end' => '2019-07-14 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'more_day_all_day_entry_daily_recurrence_every_six_days_with_end_date_and_amount' => [
                'calendarEntry' => $calendarMoreDayAllDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 6,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-11 00:00:00',
                    'until_recurrence_amount' => 2
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 00:00:00',
                        'end' => '2019-07-02 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 00:00:00',
                        'end' => '2019-07-08 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-08 00:00:00',
                        'end' => '2019-07-14 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'more_day_all_day_entry_daily_recurrence_every_six_days_with_end_date_and_greater_amount' => [
                'calendarEntry' => $calendarMoreDayAllDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 6,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-11 00:00:00',
                    'until_recurrence_amount' => 5
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 00:00:00',
                        'end' => '2019-07-02 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 00:00:00',
                        'end' => '2019-07-08 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-08 00:00:00',
                        'end' => '2019-07-14 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'more_day_all_day_entry_daily_recurrence_every_six_days_with_end_date_and_smaller_amount' => [
                'calendarEntry' => $calendarMoreDayAllDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 6,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-11 00:00:00',
                    'until_recurrence_amount' => 1
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 00:00:00',
                        'end' => '2019-07-02 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 00:00:00',
                        'end' => '2019-07-08 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],

            /**
             * Weekly test cases
             */
            'normal_entry_weekly_recurrence_sunday' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::WEEKLY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 1, // => Su
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 1
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-30 12:13:00',
                        'end' => '2019-06-30 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-07 12:13:00',
                        'end' => '2019-07-07 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal_entry_weekly_recurrence_wednesday_starting_wednesday' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::WEEKLY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 8,
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 1
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-03 12:13:00',
                        'end' => '2019-07-03 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal_entry_weekly_recurrence_every_day_but_thursday' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::WEEKLY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 111,
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 8
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-28 12:13:00',
                        'end' => '2019-06-28 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-29 12:13:00',
                        'end' => '2019-06-29 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-30 12:13:00',
                        'end' => '2019-06-30 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-01 12:13:00',
                        'end' => '2019-07-01 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-03 12:13:00',
                        'end' => '2019-07-03 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-05 12:13:00',
                        'end' => '2019-07-05 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-06 12:13:00',
                        'end' => '2019-07-06 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],

            'normal_entry_weekly_recurrence_every_day_but_thursday_8_times' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::WEEKLY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 111,
                    'by_day_of_month' => 0,
                    'until_date' => '2020-01-01 00:00:00',
                    'until_recurrence_amount' => 7
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-28 12:13:00',
                        'end' => '2019-06-28 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-29 12:13:00',
                        'end' => '2019-06-29 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-30 12:13:00',
                        'end' => '2019-06-30 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-01 12:13:00',
                        'end' => '2019-07-01 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-03 12:13:00',
                        'end' => '2019-07-03 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-05 12:13:00',
                        'end' => '2019-07-05 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal_entry_weekly_recurrence_every_day_but_thursday_til_2019-07-03' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::WEEKLY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 111,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-03 12:13:00',
                    'until_recurrence_amount' => 8
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-28 12:13:00',
                        'end' => '2019-06-28 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-29 12:13:00',
                        'end' => '2019-06-29 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-30 12:13:00',
                        'end' => '2019-06-30 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-01 12:13:00',
                        'end' => '2019-07-01 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-03 12:13:00',
                        'end' => '2019-07-03 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],

            'normal_entry_weekly_recurrence_3_days_two_recurrence' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::WEEKLY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 7, // => Su, Mo, Tu
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 2
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-30 12:13:00',
                        'end' => '2019-06-30 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-01 12:13:00',
                        'end' => '2019-07-01 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal_entry_weekly_2_recurrences_every_2nd_sunday' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::WEEKLY_RECURRENCE,
                    'interval' => 2,
                    'by_day_of_week' => 1,
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 2
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-30 12:13:00',
                        'end' => '2019-06-30 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-14 12:13:00',
                        'end' => '2019-07-14 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-28 12:13:00',
                        'end' => '2019-07-28 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],

            /**
             * Monthly recurrences
             */
            'normal_entry_1_monthly_recurrence_1_of_month' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::MONTHLY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 1,
                    'until_date' => null,
                    'until_recurrence_amount' => 1
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-07-01 12:13:00',
                        'end' => '2019-07-01 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-08-01 12:13:00',
                        'end' => '2019-08-01 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal_entry_1_monthly_recurrence_26_of_month' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::MONTHLY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 33_554_432, // => 26
                    'until_date' => null,
                    'until_recurrence_amount' => 1
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-26 12:13:00',
                        'end' => '2019-07-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],

            'normal_entry_5_monthly_recurrences_of_month' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::MONTHLY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 268_828_674, // => 2, 18, 19, 29
                    'until_date' => null,
                    'until_recurrence_amount' => 5
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-29 12:13:00',
                        'end' => '2019-06-29 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-18 12:13:00',
                        'end' => '2019-07-18 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-19 12:13:00',
                        'end' => '2019-07-19 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-29 12:13:00',
                        'end' => '2019-07-29 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-08-02 12:13:00',
                        'end' => '2019-08-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],

            'normal_entry_5_monthly_recurrences_of_month_with_incl_end_date' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::MONTHLY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 268_828_674, // => 2, 18, 19, 29
                    'until_date' => '2019-07-19 12:13:00',
                    'until_recurrence_amount' => 5
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-29 12:13:00',
                        'end' => '2019-06-29 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-18 12:13:00',
                        'end' => '2019-07-18 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-19 12:13:00',
                        'end' => '2019-07-19 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],

            'normal_entry_5_monthly_recurrences_of_month_with_excl_end_date' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::MONTHLY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 268_828_674, // => 2, 18, 19, 29
                    'until_date' => '2019-07-18 12:13:00',
                    'until_recurrence_amount' => 5
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-29 12:13:00',
                        'end' => '2019-06-29 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-18 12:13:00',
                        'end' => '2019-07-18 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],

            'normal_entry_4_monthly_recurrences_26_of_month_every_3rd_month' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::MONTHLY_RECURRENCE,
                    'interval' => 3,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 33_554_432, // => 26
                    'until_date' => null,
                    'until_recurrence_amount' => 4
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-09-26 12:13:00',
                        'end' => '2019-09-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-12-26 12:13:00',
                        'end' => '2019-12-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-03-26 12:13:00',
                        'end' => '2020-03-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-06-26 12:13:00',
                        'end' => '2020-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],

            /**
             * Yearly recurrences
             */
            'normal_entry_1_yearly_recurrence' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::YEARLY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 1
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-06-26 12:13:00',
                        'end' => '2020-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],

            'normal_entry_5_yearly_recurrences' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::YEARLY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 5
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-06-26 12:13:00',
                        'end' => '2020-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2021-06-26 12:13:00',
                        'end' => '2021-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2022-06-26 12:13:00',
                        'end' => '2022-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2023-06-26 12:13:00',
                        'end' => '2023-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2024-06-26 12:13:00',
                        'end' => '2024-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                ]
            ],

            'normal_entry_5_yearly_recurrences_with_incl_end_date' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::YEARLY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2022-06-26 12:13:00',
                    'until_recurrence_amount' => 5
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-06-26 12:13:00',
                        'end' => '2020-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2021-06-26 12:13:00',
                        'end' => '2021-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2022-06-26 12:13:00',
                        'end' => '2022-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],

            'normal_entry_5_yearly_recurrences_with_excl_end_date' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::YEARLY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2022-06-26 12:12:59',
                    'until_recurrence_amount' => 5
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-06-26 12:13:00',
                        'end' => '2020-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2021-06-26 12:13:00',
                        'end' => '2021-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],

            'normal_entry_4_yearly_recurrences_every_3rd_year' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::YEARLY_RECURRENCE,
                    'interval' => 3,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 4
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2022-06-26 12:13:00',
                        'end' => '2022-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2025-06-26 12:13:00',
                        'end' => '2025-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2028-06-26 12:13:00',
                        'end' => '2028-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2031-06-26 12:13:00',
                        'end' => '2031-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],

                ]
            ],
            'normal entry, daily_recurrence, exception do not match' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-02 12:13:00',
                    'until_recurrence_amount' => 0,
                    'exception' => [
                        [
                            'uid' => 24,
                            'start' => '2019-06-29 10:00:00',
                            'end' => '2019-06-29 12:00:01',
                            'all_day' => 0,
                        ]
                    ]
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-27 12:13:00',
                        'end' => '2019-06-27 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-28 12:13:00',
                        'end' => '2019-06-28 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-29 12:13:00',
                        'end' => '2019-06-29 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-30 12:13:00',
                        'end' => '2019-06-30 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-01 12:13:00',
                        'end' => '2019-07-01 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal entry, daily_recurrence, exception starts after event' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-02 12:13:00',
                    'until_recurrence_amount' => 0,
                    'exception' => [
                        [
                            'uid' => 24,
                            'start' => '2019-06-29 13:00:00',
                            'end' => '2019-06-29 18:00:01',
                            'all_day' => 0,
                        ]
                    ]
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-27 12:13:00',
                        'end' => '2019-06-27 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-28 12:13:00',
                        'end' => '2019-06-28 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-30 12:13:00',
                        'end' => '2019-06-30 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-01 12:13:00',
                        'end' => '2019-07-01 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal entry, daily_recurrence, exception starts before event' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-02 12:13:00',
                    'until_recurrence_amount' => 0,
                    'exception' => [
                        [
                            'uid' => 24,
                            'start' => '2019-06-29 11:00:00',
                            'end' => '2019-06-29 15:00:01',
                            'all_day' => 0,
                        ]
                    ]
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-27 12:13:00',
                        'end' => '2019-06-27 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-28 12:13:00',
                        'end' => '2019-06-28 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-30 12:13:00',
                        'end' => '2019-06-30 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-01 12:13:00',
                        'end' => '2019-07-01 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal entry, daily_recurrence, exception greater than event' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-02 12:13:00',
                    'until_recurrence_amount' => 0,
                    'exception' => [
                        [
                            'uid' => 24,
                            'start' => '2019-06-29 11:00:00',
                            'end' => '2019-06-29 18:00:01',
                            'all_day' => 0,
                        ]
                    ]
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-27 12:13:00',
                        'end' => '2019-06-27 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-28 12:13:00',
                        'end' => '2019-06-28 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-30 12:13:00',
                        'end' => '2019-06-30 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-01 12:13:00',
                        'end' => '2019-07-01 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal entry, daily_recurrence, exception within event' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-02 12:13:00',
                    'until_recurrence_amount' => 0,
                    'exception' => [
                        [
                            'uid' => 24,
                            'start' => '2019-06-29 13:00:00',
                            'end' => '2019-06-29 13:00:01',
                            'all_day' => 0,
                        ]
                    ]
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-27 12:13:00',
                        'end' => '2019-06-27 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-28 12:13:00',
                        'end' => '2019-06-28 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-30 12:13:00',
                        'end' => '2019-06-30 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-01 12:13:00',
                        'end' => '2019-07-01 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal entry, daily_recurrence, exception covers tree events' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-02 12:13:00',
                    'until_recurrence_amount' => 0,
                    'exception' => [
                        [
                            'uid' => 24,
                            'start' => '2019-06-28 13:00:00',
                            'end' => '2019-06-30 15:00:01',
                            'all_day' => 0,
                        ]
                    ]
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-27 12:13:00',
                        'end' => '2019-06-27 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-01 12:13:00',
                        'end' => '2019-07-01 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal entry, daily_recurrence, exception all_day' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-02 12:13:00',
                    'until_recurrence_amount' => 0,
                    'exception' => [
                        [
                            'uid' => 24,
                            'start' => '2019-06-29 12:00:00',
                            'end' => '2019-06-29 12:00:01',
                            'all_day' => 1,
                        ]
                    ]
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-27 12:13:00',
                        'end' => '2019-06-27 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-28 12:13:00',
                        'end' => '2019-06-28 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-30 12:13:00',
                        'end' => '2019-06-30 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-01 12:13:00',
                        'end' => '2019-07-01 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal entry, 7 daily_recurrence, exception starts after event' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '0',
                    'until_recurrence_amount' => 6,
                    'exception' => [
                        [
                            'uid' => 24,
                            'start' => '2019-06-29 13:00:00',
                            'end' => '2019-06-29 18:00:01',
                            'all_day' => 0,
                        ]
                    ]
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-27 12:13:00',
                        'end' => '2019-06-27 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-28 12:13:00',
                        'end' => '2019-06-28 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-30 12:13:00',
                        'end' => '2019-06-30 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-01 12:13:00',
                        'end' => '2019-07-01 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'all day entry, daily_recurrence, exception within day' => [
                'calendarEntry' => $calendarAllDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-02 12:13:00',
                    'until_recurrence_amount' => 0,
                    'exception' => [
                        [
                            'uid' => 24,
                            'start' => '2019-06-29 12:00:00',
                            'end' => '2019-06-29 12:00:01',
                            'all_day' => 0,
                        ]
                    ]
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 00:00:00',
                        'end' => '2019-06-26 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-27 00:00:00',
                        'end' => '2019-06-27 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-28 00:00:00',
                        'end' => '2019-06-28 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-30 00:00:00',
                        'end' => '2019-06-30 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-01 00:00:00',
                        'end' => '2019-07-01 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 00:00:00',
                        'end' => '2019-07-02 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'all day entry, daily_recurrence, exception all day' => [
                'calendarEntry' => $calendarAllDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-02 12:13:00',
                    'until_recurrence_amount' => 0,
                    'exception' => [
                        [
                            'uid' => 24,
                            'start' => '2019-06-29 12:00:00',
                            'end' => '2019-06-29 12:00:01',
                            'all_day' => 1,
                        ]
                    ]
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 00:00:00',
                        'end' => '2019-06-26 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-27 00:00:00',
                        'end' => '2019-06-27 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-28 00:00:00',
                        'end' => '2019-06-28 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-30 00:00:00',
                        'end' => '2019-06-30 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-01 00:00:00',
                        'end' => '2019-07-01 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 00:00:00',
                        'end' => '2019-07-02 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal entry, daily_recurrence, two exceptions' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-02 12:13:00',
                    'until_recurrence_amount' => 0,
                    'exception' => [
                        [
                            'uid' => 24,
                            'start' => '2019-06-29 11:00:00',
                            'end' => '2019-06-29 18:00:01',
                            'all_day' => 0,
                        ],
                        [
                            'uid' => 25,
                            'start' => '2019-07-01 13:00:00',
                            'end' => '2019-07-01 13:00:01',
                            'all_day' => 0,
                        ]
                    ]
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-27 12:13:00',
                        'end' => '2019-06-27 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-28 12:13:00',
                        'end' => '2019-06-28 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-30 12:13:00',
                        'end' => '2019-06-30 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'more_day_entry_5_daily_recurrence_every_three_days with simple exception' => [
                'calendarEntry' => $calendarMoreDayEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 3,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 5,
                    'exception' => [
                        [
                            'uid' => 24,
                            'start' => '2019-07-08 18:00:00',
                            'end' => '2019-07-08 18:00:01',
                            'all_day' => 0,
                        ]
                    ]
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-29 12:13:00',
                        'end' => '2019-07-05 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-08 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-11 12:13:00',
                        'end' => '2019-07-17 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal entry, daily_recurrence, single deviation ' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-02 12:13:00',
                    'until_recurrence_amount' => 0,
                    'deviation' => [
                        [
                            'uid' => 24,
                            'deviated_event_start' => '2019-06-29 12:13:00',
                            'start' => '2019-06-29 10:00:00',
                            'end' => '2019-06-29 13:00:00',
                            'all_day' => 0,
                        ]
                    ]
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-27 12:13:00',
                        'end' => '2019-06-27 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-28 12:13:00',
                        'end' => '2019-06-28 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-29 10:00:00',
                        'end' => '2019-06-29 13:00:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 24,
                    ],
                    [
                        'start' => '2019-06-30 12:13:00',
                        'end' => '2019-06-30 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-01 12:13:00',
                        'end' => '2019-07-01 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal entry, daily_recurrence, single all-day deviation ' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-02 12:13:00',
                    'until_recurrence_amount' => 0,
                    'deviation' => [
                        [
                            'uid' => 24,
                            'deviated_event_start' => '2019-06-29 12:13:00',
                            'start' => '2019-06-29 10:00:00',
                            'end' => '2019-06-29 13:00:00',
                            'all_day' => 1,
                        ]
                    ]
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-27 12:13:00',
                        'end' => '2019-06-27 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-28 12:13:00',
                        'end' => '2019-06-28 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-29 00:00:00',
                        'end' => '2019-06-29 23:59:59',
                        'original_entry_uid' => 23,
                        'entry_uid' => 24,
                    ],
                    [
                        'start' => '2019-06-30 12:13:00',
                        'end' => '2019-06-30 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-01 12:13:00',
                        'end' => '2019-07-01 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal entry, daily_recurrence, non matching deviation ' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-02 12:13:00',
                    'until_recurrence_amount' => 0,
                    'deviation' => [
                        [
                            'uid' => 24,
                            'deviated_event_start' => '2019-06-29 12:13:01',
                            'start' => '2019-06-29 10:00:00',
                            'end' => '2019-06-29 13:00:00',
                            'all_day' => 0,
                        ]
                    ]
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-27 12:13:00',
                        'end' => '2019-06-27 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-28 12:13:00',
                        'end' => '2019-06-28 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-29 12:13:00',
                        'end' => '2019-06-29 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-30 12:13:00',
                        'end' => '2019-06-30 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-01 12:13:00',
                        'end' => '2019-07-01 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal entry, daily_recurrence, two deviations ' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::DAILY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-07-02 12:13:00',
                    'until_recurrence_amount' => 0,
                    'deviation' => [
                        [
                            'uid' => 24,
                            'deviated_event_start' => '2019-06-29 12:13:00',
                            'start' => '2019-06-29 10:00:00',
                            'end' => '2019-06-29 13:00:00',
                            'all_day' => 0,
                        ],
                        [
                            'uid' => 25,
                            'deviated_event_start' => '2019-07-01 12:13:00',
                            'start' => '2019-07-01 15:00:00',
                            'end' => '2019-07-01 19:00:00',
                            'all_day' => 0,
                        ]

                    ]
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-27 12:13:00',
                        'end' => '2019-06-27 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-28 12:13:00',
                        'end' => '2019-06-28 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-06-29 10:00:00',
                        'end' => '2019-06-29 13:00:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 24,
                    ],
                    [
                        'start' => '2019-06-30 12:13:00',
                        'end' => '2019-06-30 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-01 15:00:00',
                        'end' => '2019-07-01 19:00:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 25,
                    ],
                    [
                        'start' => '2019-07-02 12:13:00',
                        'end' => '2019-07-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal entry, monthly_recurrence, every second friday ' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::MONTHLY_RECURRENCE,
                    'interval' => 0,
                    'by_day_of_week' => 0,
                    'by_day_index' => 2,
                    'by_day_day' => 6,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-09-23 12:13:00',
                    'until_recurrence_amount' => 0,
                    'deviation' => []
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-07-12 12:13:00',
                        'end' => '2019-07-12 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-08-09 12:13:00',
                        'end' => '2019-08-09 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-09-13 12:13:00',
                        'end' => '2019-09-13 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal entry, monthly_recurrence, every second friday, with turn of the year ' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::MONTHLY_RECURRENCE,
                    'interval' => 0,
                    'by_day_of_week' => 0,
                    'by_day_index' => 2,
                    'by_day_day' => 6,
                    'by_day_of_month' => 0,
                    'until_date' => '2020-03-01 12:13:00',
                    'until_recurrence_amount' => 0,
                    'deviation' => []
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-07-12 12:13:00',
                        'end' => '2019-07-12 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-08-09 12:13:00',
                        'end' => '2019-08-09 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-09-13 12:13:00',
                        'end' => '2019-09-13 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-10-11 12:13:00',
                        'end' => '2019-10-11 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-11-08 12:13:00',
                        'end' => '2019-11-08 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-12-13 12:13:00',
                        'end' => '2019-12-13 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-01-10 12:13:00',
                        'end' => '2020-01-10 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-02-14 12:13:00',
                        'end' => '2020-02-14 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal entry, monthly_recurrence, every third to last friday ' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::MONTHLY_RECURRENCE,
                    'interval' => 0,
                    'by_day_of_week' => 0,
                    'by_day_index' => -3,
                    'by_day_day' => 6,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-09-23 12:13:00',
                    'until_recurrence_amount' => 0,
                    'deviation' => []
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-07-12 12:13:00',
                        'end' => '2019-07-12 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-08-16 12:13:00',
                        'end' => '2019-08-16 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-09-13 12:13:00',
                        'end' => '2019-09-13 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal entry, monthly_recurrence, every fifth friday ' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::MONTHLY_RECURRENCE,
                    'interval' => 0,
                    'by_day_of_week' => 0,
                    'by_day_index' => 5,
                    'by_day_day' => 6,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-09-23 12:13:00',
                    'until_recurrence_amount' => 0,
                    'deviation' => []
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-07-05 12:13:00',
                        'end' => '2019-07-05 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-08-02 12:13:00',
                        'end' => '2019-08-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-08-30 12:13:00',
                        'end' => '2019-08-30 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal entry, monthly_recurrence, every fifth friday with fallback to last' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::MONTHLY_RECURRENCE,
                    'interval' => 0,
                    'by_day_of_week' => 0,
                    'by_day_index' => 6,
                    'by_day_day' => 6,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-09-23 12:13:00',
                    'until_recurrence_amount' => 0,
                    'deviation' => []
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-28 12:13:00',
                        'end' => '2019-06-28 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-26 12:13:00',
                        'end' => '2019-07-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-08-30 12:13:00',
                        'end' => '2019-08-30 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal entry, monthly_recurrence, every fifth to last friday with fallback to last' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::MONTHLY_RECURRENCE,
                    'interval' => 0,
                    'by_day_of_week' => 0,
                    'by_day_index' => -6,
                    'by_day_day' => 6,
                    'by_day_of_month' => 0,
                    'until_date' => '2019-09-23 12:13:00',
                    'until_recurrence_amount' => 0,
                    'deviation' => []
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-07-05 12:13:00',
                        'end' => '2019-07-05 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-08-02 12:13:00',
                        'end' => '2019-08-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-09-06 12:13:00',
                        'end' => '2019-09-06 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal entry, monthly_recurrence, every fifth friday with fallback to last, with turn of the year ' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::MONTHLY_RECURRENCE,
                    'interval' => 0,
                    'by_day_of_week' => 0,
                    'by_day_index' => 6,
                    'by_day_day' => 6,
                    'by_day_of_month' => 0,
                    'until_date' => '2020-03-01 12:13:00',
                    'until_recurrence_amount' => 0,
                    'deviation' => []
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-28 12:13:00',
                        'end' => '2019-06-28 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-26 12:13:00',
                        'end' => '2019-07-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-08-30 12:13:00',
                        'end' => '2019-08-30 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-09-27 12:13:00',
                        'end' => '2019-09-27 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-10-25 12:13:00',
                        'end' => '2019-10-25 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-11-29 12:13:00',
                        'end' => '2019-11-29 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-12-27 12:13:00',
                        'end' => '2019-12-27 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-01-31 12:13:00',
                        'end' => '2020-01-31 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-02-28 12:13:00',
                        'end' => '2020-02-28 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ],
            ],
            'normal entry, monthly_recurrence, every fifth to last friday with fallback to first, with turn of the year ' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::MONTHLY_RECURRENCE,
                    'interval' => 0,
                    'by_day_of_week' => 0,
                    'by_day_index' => -6,
                    'by_day_day' => 6,
                    'by_day_of_month' => 0,
                    'until_date' => '2020-03-01 12:13:00',
                    'until_recurrence_amount' => 0,
                    'deviation' => []
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-07-05 12:13:00',
                        'end' => '2019-07-05 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-08-02 12:13:00',
                        'end' => '2019-08-02 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-09-06 12:13:00',
                        'end' => '2019-09-06 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-10-04 12:13:00',
                        'end' => '2019-10-04 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-11-01 12:13:00',
                        'end' => '2019-11-01 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-12-06 12:13:00',
                        'end' => '2019-12-06 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-01-03 12:13:00',
                        'end' => '2020-01-03 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-02-07 12:13:00',
                        'end' => '2020-02-07 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
            'normal entry, monthly_recurrence, every fifth friday with fallback to last, with turn of two years ' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::MONTHLY_RECURRENCE,
                    'interval' => 0,
                    'by_day_of_week' => 0,
                    'by_day_index' => 6,
                    'by_day_day' => 6,
                    'by_day_of_month' => 0,
                    'until_date' => '2021-03-01 12:13:00',
                    'until_recurrence_amount' => 0,
                    'deviation' => []
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-28 12:13:00',
                        'end' => '2019-06-28 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-07-26 12:13:00',
                        'end' => '2019-07-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-08-30 12:13:00',
                        'end' => '2019-08-30 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-09-27 12:13:00',
                        'end' => '2019-09-27 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-10-25 12:13:00',
                        'end' => '2019-10-25 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-11-29 12:13:00',
                        'end' => '2019-11-29 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2019-12-27 12:13:00',
                        'end' => '2019-12-27 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-01-31 12:13:00',
                        'end' => '2020-01-31 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-02-28 12:13:00',
                        'end' => '2020-02-28 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-03-27 12:13:00',
                        'end' => '2020-03-27 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-04-24 12:13:00',
                        'end' => '2020-04-24 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-05-29 12:13:00',
                        'end' => '2020-05-29 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-06-26 12:13:00',
                        'end' => '2020-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-07-31 12:13:00',
                        'end' => '2020-07-31 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-08-28 12:13:00',
                        'end' => '2020-08-28 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-09-25 12:13:00',
                        'end' => '2020-09-25 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-10-30 12:13:00',
                        'end' => '2020-10-30 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-11-27 12:13:00',
                        'end' => '2020-11-27 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-12-25 12:13:00',
                        'end' => '2020-12-25 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2021-01-29 12:13:00',
                        'end' => '2021-01-29 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2021-02-26 12:13:00',
                        'end' => '2021-02-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ],
            ],
        ];
    }

    /**
     * @param array $calendarEntry
     * @param array $recurrenceDatabaseRow
     * @param array $expectedEventDatabaseRows
     * @dataProvider calculateEventRowsDataProvider
     */
    public function testCalculateEventRows(
        array $calendarEntry,
        array $recurrenceDatabaseRow,
        array $expectedEventDatabaseRows
    ): void {
        $this->resetSingletonInstances = true;
        $eventService = new EventService();
        // Effectively disabling the untilDate cap.
        $eventService->setUntilDate(new DateTime('2099-06-26 12:12:59'));
        self::assertEquals(
            $expectedEventDatabaseRows,
            $eventService->calculateEventRows($calendarEntry, $recurrenceDatabaseRow)
        );
    }

    /**
     * @return array
     */
    public function untilDateCapDataProvider(): array
    {
        $calendarEntryDatabaseRow = [
            'uid' => 23,
            'start' => '2019-06-26 12:13:00',
            'end' => '2019-06-26 17:47:00',
            'all_day' => 0,
        ];

        return [
            'normal_entry_1_yearly_recurrence' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::YEARLY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 1
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-06-26 12:13:00',
                        'end' => '2020-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],

            'normal_entry_5_yearly_recurrences' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::YEARLY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 5
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-06-26 12:13:00',
                        'end' => '2020-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2021-06-26 12:13:00',
                        'end' => '2021-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2022-06-26 12:13:00',
                        'end' => '2022-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],

            'normal_entry_5_yearly_recurrences_with_incl_end_date' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::YEARLY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2022-06-26 12:13:00',
                    'until_recurrence_amount' => 5
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-06-26 12:13:00',
                        'end' => '2020-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2021-06-26 12:13:00',
                        'end' => '2021-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2022-06-26 12:13:00',
                        'end' => '2022-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],

            'normal_entry_5_yearly_recurrences_with_excl_end_date' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::YEARLY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => '2022-06-26 12:12:59',
                    'until_recurrence_amount' => 5
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-06-26 12:13:00',
                        'end' => '2020-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2021-06-26 12:13:00',
                        'end' => '2021-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],

            'normal_entry_4_yearly_recurrences_every_3rd_year' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::YEARLY_RECURRENCE,
                    'interval' => 3,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 4
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2022-06-26 12:13:00',
                        'end' => '2022-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],

            'normal_entry_yearly_recurrence_without_until_border' => [
                'calendarEntry' => $calendarEntryDatabaseRow,
                'recurrenceDatabaseRow' => [
                    'original_entry_uid' => 23,
                    'entry_uid' => 23,
                    'frequency' => EventService::YEARLY_RECURRENCE,
                    'interval' => 1,
                    'by_day_of_week' => 0,
                    'by_day_of_month' => 0,
                    'until_date' => null,
                    'until_recurrence_amount' => 0
                ],
                'expectedEventDatabaseRows' => [
                    [
                        'start' => '2019-06-26 12:13:00',
                        'end' => '2019-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2020-06-26 12:13:00',
                        'end' => '2020-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2021-06-26 12:13:00',
                        'end' => '2021-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ],
                    [
                        'start' => '2022-06-26 12:13:00',
                        'end' => '2022-06-26 17:47:00',
                        'original_entry_uid' => 23,
                        'entry_uid' => 23,
                    ]
                ]
            ],
        ];
    }

    /**
     * @param array $calendarEntry
     * @param array $recurrenceDatabaseRow
     * @param array $expectedEventDatabaseRows
     * @throws Exception
     * @dataProvider untilDateCapDataProvider
     */
    public function testWorkingUntilDateCap(
        array $calendarEntry,
        array $recurrenceDatabaseRow,
        array $expectedEventDatabaseRows
    ): void {
        $this->resetSingletonInstances = true;
        $eventService = new EventService();
        $eventService->setUntilDate(new DateTime('2023-06-26 12:12:59'));

        self::assertEquals(
            $expectedEventDatabaseRows,
            $eventService->calculateEventRows($calendarEntry, $recurrenceDatabaseRow)
        );
    }
}
