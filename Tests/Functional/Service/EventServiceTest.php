<?php
declare(strict_types=1);

namespace JanHelke\CalendarFoundation\Tests\Functional;

use JanHelke\CalendarFoundation\Service\EventService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\TestingFramework\Core\Functional\FunctionalTestCase;

/**
 * Class MigrateCalEntriesServiceFunctionalTest
 */
class EventServiceTest extends FunctionalTestCase
{
    /**
     * @var string[]
     */
    protected $testExtensionsToLoad = [
        'typo3conf/ext/calendar_foundation',
    ];

    public function testCreateAndSaveEventIndex(): void
    {
        $this->importCSVDataSet(GeneralUtility::getFileAbsFileName('EXT:calendar_foundation/Tests/Functional/Fixtures/event_test_calendar_entries.csv'));
        (new EventService())->createAndSaveEventIndex();
        $this->assertCSVDataSet('EXT:calendar_foundation/Tests/Functional/Fixtures/event_test_events.csv');
    }
}
