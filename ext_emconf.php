<?php

$EM_CONF['calendar_foundation'] = [
    'title' => 'Calendar Foundation',
    'description' => 'The foundation for the calendar. At the core it is the successor of ext:cal.',
    'category' => 'be',
    'version' => '1.0.0',
    'state' => 'stable',
    'author' => 'Jan Helke',
    'author_email' => 'calendar@typo3.helke.de',
    'constraints' => [
        'depends' => [
            'typo3' => '*'
        ],
    ]
];
