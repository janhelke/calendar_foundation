# TYPO3 Extension `calendar_framework`

The calendar_framework is a couple of separate extensions, that work perfectly hand in hand to replace the very well matured `cal` extension.

## Features

*   Full replacement for `cal` extension (incl. migration extension)
*   Separate extensions for separate areas of concern
*   Covers all current TYPO3 LTS versions as well as the current master branch
*   Test driven developed
*   Well documented

## Wording

To distinguish a couple of terms within the calender_framework, the following definitions will apply:

*   **Entry**: An entry is one row in the table tx_calendar_entry and defines all "metadata" of one or many entries. Due to connection with one ore more recurrences one entry can result in many events.
*   **Event**: An event is a concrete representation of the combination of either an entry without any recurrences or the combination of an entry with it recurrences. Every event is stored in the table tx_calendar_event as an own row with only the concrete start and end of this event and the connection to the entry (to have an easy to follow association to all the metadata like "title" or "description").
*   **Recurrence**: When an event takes place more than once, this is represented by the recurrences. In the table tx_calendar_recurrences every row is connected to only one entry and can contain information like "every 2nd day", "every monday" or "every 3rd week"
*   **Deviation**: If you have a single event during a recurrence that deviates from the default event. A typical deviation might be a changed room for a gathering or a different start and end time.
*   **Exception**: Time periods within a recurrence when the events don't take place. Think of closed gymnasiums during the holidays or vacation of a trainer.
    *    Exceptions can be used for multiple events ("all courses will be canceled during the holidays") and can have an own recurrence ("every year all courses will be canceled on halloween").
    *    **Exception** vs. **Deviation**: If you have an event, that is canceled for some reason (exception), but have a deviation matching the same date, the the deviation will take precedence. Think about the following use case: "During the holidays we have to cancel all courses in the regular location (exception). Therefore we evade to another location (deviation)."
*   **Exception groups**: For convenience reasons, it is possible to group exceptions in useful units. E.g. group all holidays in one group to apply this group to multiple entries. Please note, that this groups act only as a kind of shrink wrap for exceptions with no additional function. So every time, an exception group is used within the calender_framework the list of associated exceptions is returned instead.

## Players in the calendar_framework micro cosmos

As mentioned above, the calendar_framework consists of a lot of different extensions to cluster different areas in different extensions and not clutter one single extension.

### "Core" extensions

Not as in "TYPO3 core" but as in "you'll need this extensions to run your own basic calendar".

*   calendar_base => A wrapper or package extension to bundle everything you need to add a basic calendar to your website. Contains all core extensions as well as the calendar_migration (for the time being).
*   calendar_foundation => The very heard of the calendar_framework. Everything backend related about entering new entries, indexing them into real events (incl. exceptions and derivations) and group stuff together in calendars.
*   calendar_api => The first point to connect to the calendar_framework from the outside (as in "another server" or "another application").
*   calendar_frontend => A very basic implementation of a frontend interface. Contains a rough list as well as daily, weekly, monthly and yearly view. As this extension makes use heavily of the calendar_api extension, it is a perfect extension to start your own customized frontend.

### Adjecent extensions

A lot of stuff can be considered as a "must have" for any calendar. But only for a very small range of users. So these feature extensions will kept separately to keep the core extensions slim and maintainable.

*   calendar_migration => This extension is a courtesy to all of you out there using currently cal and want to migrate to a reliable new calendar. Is aims to migrate almost everything over from a of-the-rack cal installation (as long as the feature is implemented somewhere within the calendar_framework). But please keep in mind, that no extension will every be able to migrate everything of your own customized cal-extending extensions. But at least it is a good start and you are encouraged to build your own migration wizards. For the time being I will handle this extension like a core extension to make your transition from cal to the calendar_framework as smooth as possible.
*   calendar_icalendar => Ths extension aims to handle everything related to iCalendar issues. As not everyone needs this functionality, I decided to put it in an own extension. This is currently heavily WIP and will not be tackled (at least not from my side) before I am satisfied with the core extensions.

## Wild ideas

This extensions are merely "would be nice to have it sometimes" ideas. Nothing concrete is already done in those areas (in fact not even the extension keys in the TER are registered. So if you are eager to help out there - feel free to do so.

*   calendar_registration => Would be a nice idea to enable frontend users to register for a given event.
*   calendar_address => Would be a nice idea to add addresses to events. Not sure if it would be more clever to use tt_address for this.
*   calendar_frontend_editing => Would be a nice idea to have the possibility to create and edit entries from the frontend.

## Administration corner

### Versions and support

| calendar_foundation | TYPO3       | PHP       | Support/Development                     |
| ------------------- | ----------- | ----------|---------------------------------------- |
| 1.x                 | 8.7 - 10.x  | 7.0 - 7.2 | Features, Bugfixes, Security Updates    |

### Changelog

Please look into the [official extension documentation in changelog chapter](https://docs.typo3.org/typo3cms/drafts/github/janhelke/calendar_foundation/Misc/Changelog/Index.html)

### Release Management

The calendar_framework uses **semantic versioning** which basically means for you, that

*   **bugfix updates** (e.g. 1.0.0 => 1.0.1) just includes small bugfixes or security relevant stuff without breaking changes.
*   **minor updates** (e.g. 1.0.0 => 1.1.0) includes new features and smaller tasks without breaking changes.
*   **major updates** (e.g. 1.0.0 => 2.0.0) breaking changes which can be refactorings, features or bugfixes.

### Contribution

**Pull requests** are welcome in general! Nevertheless please don't forget to add an issue and connect it to your pull requests. This is very helpful to understand what kind of issue the **PR** is going to solve.

*   Bugfixes: Please describe what kind of bug your fix solve and give us feedback how to reproduce the issue. We're going to accept only bugfixes if I can reproduce the issue.
*   Features: Not every feature is relevant for the bulk of `calendar_foundation` users. In addition: We don't want to make `calendar_foundation` even more complicated in usability for an edge case feature. Please discuss a new feature before.
