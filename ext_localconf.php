<?php

use JanHelke\CalendarFoundation\Hook\DataHandlerHook;

(static function () {
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass']['calendar_foundation'] = DataHandlerHook::class;

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks'][\JanHelke\CalendarFoundation\Task\IndexerTask::class] = [
        'extension' => 'calendar_foundation',
        'title' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang.xlf:indexer_task.name',
        'description' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang.xlf:indexer_task.description',
        'additionalFields' => \JanHelke\CalendarFoundation\Task\IndexerTask::class
    ];
})();
