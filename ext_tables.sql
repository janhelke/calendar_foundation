CREATE TABLE tx_calendar_entry
(
    title varchar(128) DEFAULT '' NOT NULL,
    teaser text,
    description text,
    start TIMESTAMP,
    end TIMESTAMP,
    all_day tinyint(1) DEFAULT 0 NOT NULL,
    recurrence int(11) DEFAULT 0 NOT NULL,
    calendar int(11) DEFAULT 0 NOT NULL,

# Legacy table definitions for use with TYPO3 8 LTS.
# Remove together with the support for this version.
    uid int(10) unsigned NOT NULL AUTO_INCREMENT,
    pid int(10) unsigned NOT NULL DEFAULT '0',
    tstamp int(10) unsigned NOT NULL DEFAULT '0',
    crdate int(10) unsigned NOT NULL DEFAULT '0',
    cruser_id int(10) unsigned NOT NULL DEFAULT '0',
    deleted smallint(5) unsigned NOT NULL DEFAULT '0',
    hidden smallint(5) unsigned NOT NULL DEFAULT '0',
    starttime int(10) unsigned NOT NULL DEFAULT '0',
    endtime int(10) unsigned NOT NULL DEFAULT '0',
    sys_language_uid int(11) NOT NULL DEFAULT '0',
    l18n_parent int(10) unsigned NOT NULL DEFAULT '0',
    l10n_state text COLLATE utf8_unicode_ci,
    l18n_diffsource mediumblob,
    PRIMARY KEY (uid),
    KEY parent (pid,deleted,hidden)
);

CREATE TABLE tx_calendar_exception
(
    title varchar(128) DEFAULT '' NOT NULL,
    teaser text,
    description text,
    start TIMESTAMP,
    end TIMESTAMP,
    all_day tinyint(1) DEFAULT 0 NOT NULL,
    recurrence int(11) DEFAULT 0 NOT NULL,

# Legacy table definitions for use with TYPO3 8 LTS.
# Remove together with the support for this version.
    uid int(10) unsigned NOT NULL AUTO_INCREMENT,
    pid int(10) unsigned NOT NULL DEFAULT '0',
    tstamp int(10) unsigned NOT NULL DEFAULT '0',
    crdate int(10) unsigned NOT NULL DEFAULT '0',
    cruser_id int(10) unsigned NOT NULL DEFAULT '0',
    deleted smallint(5) unsigned NOT NULL DEFAULT '0',
    hidden smallint(5) unsigned NOT NULL DEFAULT '0',
    starttime int(10) unsigned NOT NULL DEFAULT '0',
    endtime int(10) unsigned NOT NULL DEFAULT '0',
    sys_language_uid int(11) NOT NULL DEFAULT '0',
    l18n_parent int(10) unsigned NOT NULL DEFAULT '0',
    l10n_state text COLLATE utf8_unicode_ci,
    l18n_diffsource mediumblob,
    PRIMARY KEY (uid),
    KEY parent (pid,deleted,hidden)
);

CREATE TABLE tx_calendar_exception_group
(
    title varchar(128) DEFAULT '' NOT NULL,
    exception int(11) DEFAULT 0 NOT NULL,

# Legacy table definitions for use with TYPO3 8 LTS.
# Remove together with the support for this version.
    uid int(10) unsigned NOT NULL AUTO_INCREMENT,
    pid int(10) unsigned NOT NULL DEFAULT '0',
    tstamp int(10) unsigned NOT NULL DEFAULT '0',
    crdate int(10) unsigned NOT NULL DEFAULT '0',
    cruser_id int(10) unsigned NOT NULL DEFAULT '0',
    deleted smallint(5) unsigned NOT NULL DEFAULT '0',
    hidden smallint(5) unsigned NOT NULL DEFAULT '0',
    starttime int(10) unsigned NOT NULL DEFAULT '0',
    endtime int(10) unsigned NOT NULL DEFAULT '0',
    sys_language_uid int(11) NOT NULL DEFAULT '0',
    l18n_parent int(10) unsigned NOT NULL DEFAULT '0',
    l10n_state text COLLATE utf8_unicode_ci,
    l18n_diffsource mediumblob,
    PRIMARY KEY (uid),
    KEY parent (pid,deleted,hidden)
);

CREATE TABLE tx_calendar_deviation
(
    title varchar(128) DEFAULT '' NOT NULL,
    teaser text,
    description text,
    start TIMESTAMP,
    end TIMESTAMP,
    all_day tinyint(1) DEFAULT 0 NOT NULL,
    recurrence int(11) DEFAULT 0 NOT NULL,
    deviated_event_start TIMESTAMP,
    parent_recurrence_uid int(11) DEFAULT 0 NOT NULL,

# Legacy table definitions for use with TYPO3 8 LTS.
# Remove together with the support for this version.
    uid int(10) unsigned NOT NULL AUTO_INCREMENT,
    pid int(10) unsigned NOT NULL DEFAULT '0',
    tstamp int(10) unsigned NOT NULL DEFAULT '0',
    crdate int(10) unsigned NOT NULL DEFAULT '0',
    cruser_id int(10) unsigned NOT NULL DEFAULT '0',
    deleted smallint(5) unsigned NOT NULL DEFAULT '0',
    hidden smallint(5) unsigned NOT NULL DEFAULT '0',
    starttime int(10) unsigned NOT NULL DEFAULT '0',
    endtime int(10) unsigned NOT NULL DEFAULT '0',
    sys_language_uid int(11) NOT NULL DEFAULT '0',
    l18n_parent int(10) unsigned NOT NULL DEFAULT '0',
    l10n_state text COLLATE utf8_unicode_ci,
    l18n_diffsource mediumblob,
    PRIMARY KEY (uid),
    KEY parent (pid,deleted,hidden)
);

CREATE TABLE tx_calendar_recurrence
(
    entry int(11) DEFAULT 0 NOT NULL,
    frequency tinyint(1) DEFAULT 0 NOT NULL,
    'interval' tinyint(4) DEFAULT 0 NOT NULL,
    by_day_of_week int(11) DEFAULT 0 NOT NULL,
    by_day_index int(2) DEFAULT 0 NOT NULL,
    by_day_day int(1) DEFAULT 0 NOT NULL,
    by_day_of_month int(11) DEFAULT 0 NOT NULL,
    until_date TIMESTAMP,
    until_recurrence_amount int(11) DEFAULT 0,
    deviation int(11) DEFAULT 0 NOT NULL,
    exception int(11) DEFAULT 0 NOT NULL,
    parent_table varchar(50) DEFAULT '' NOT NULL,

# Legacy table definitions for use with TYPO3 8 LTS.
# Remove together with the support for this version.
    uid int(10) unsigned NOT NULL AUTO_INCREMENT,
    pid int(10) unsigned NOT NULL DEFAULT '0',
    tstamp int(10) unsigned NOT NULL DEFAULT '0',
    crdate int(10) unsigned NOT NULL DEFAULT '0',
    cruser_id int(10) unsigned NOT NULL DEFAULT '0',
    deleted smallint(5) unsigned NOT NULL DEFAULT '0',
    PRIMARY KEY (uid),
    KEY parent (pid,deleted)
);

CREATE TABLE tx_calendar_event
(
    uid int(10) unsigned NOT NULL AUTO_INCREMENT,
    start TIMESTAMP,
    end TIMESTAMP,
    original_entry_uid int(11) DEFAULT 0 NOT NULL,
    entry_uid int(11) DEFAULT 0 NOT NULL,
    PRIMARY KEY (uid),
    KEY start (start),
    KEY entry_start (entry_uid, start)
);

CREATE TABLE tx_calendar_calendar
(
    title varchar(128) DEFAULT '' NOT NULL,
    activate_free_and_busy tinyint(1) unsigned DEFAULT '0' NOT NULL,
    free_and_busy_users_and_groups int(11) unsigned DEFAULT '0' NOT NULL,
    type varchar(32) DEFAULT '' NOT NULL,
    ical_url text,
    ical_file VARCHAR(255) DEFAULT '' NOT NULL,
    refresh_interval int(11) unsigned DEFAULT '0' NOT NULL,
    md5 varchar(32) DEFAULT '' NOT NULL,
    scheduler_id int(11) unsigned DEFAULT '0' NOT NULL,

# Legacy table definitions for use with TYPO3 8 LTS.
# Remove together with the support for this version.
    uid int(10) unsigned NOT NULL AUTO_INCREMENT,
    pid int(10) unsigned NOT NULL DEFAULT '0',
    tstamp int(10) unsigned NOT NULL DEFAULT '0',
    crdate int(10) unsigned NOT NULL DEFAULT '0',
    cruser_id int(10) unsigned NOT NULL DEFAULT '0',
    deleted smallint(5) unsigned NOT NULL DEFAULT '0',
    hidden smallint(5) unsigned NOT NULL DEFAULT '0',
    starttime int(10) unsigned NOT NULL DEFAULT '0',
    endtime int(10) unsigned NOT NULL DEFAULT '0',
    sys_language_uid int(11) NOT NULL DEFAULT '0',
    l18n_parent int(10) unsigned NOT NULL DEFAULT '0',
    l10n_state text COLLATE utf8_unicode_ci,
    l18n_diffsource mediumblob,
    PRIMARY KEY (uid),
    KEY parent (pid,deleted,hidden)
);

CREATE TABLE tx_calendar_mm_relation
(
    uid_local int(11) unsigned DEFAULT '0' NOT NULL,
    uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
    table_local varchar(50) DEFAULT '' NOT NULL,
    tablenames varchar(50) DEFAULT '' NOT NULL,
    sorting int(11) unsigned DEFAULT '0' NOT NULL,
    KEY uid_local (uid_local),
    KEY uid_foreign (uid_foreign)
);
