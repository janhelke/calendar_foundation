<?php
declare(strict_types=1);

if (!defined('TYPO3')) {
    die('Access denied.');
}

return [
    'ctrl' => [
        'title' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_exception_group.xlf:tx_calendar_exception_group',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime'
        ],
        'transOrigPointerField' => 'l18n_parent',
        'transOrigDiffSourceField' => 'l18n_diffsource',
        'languageField' => 'sys_language_uid',
        'iconfile' => 'EXT:calendar_foundation/Resources/Public/Icons/tx_calendar_exception_group.svg',
        'searchFields' => 'title'
    ],
    'types' => [
        0 => [
            'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,title,exception,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,hidden,starttime,endtime'
        ],
    ],
    'columns' => [
        'title' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_exception_group.xlf:tx_calendar_calendar.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 128,
                'eval' => 'required'
            ]
        ],
        'exception' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_exception_group.xlf:tx_calendar_exception_group.exception',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_calendar_exception',
                'MM' => 'tx_calendar_mm_relation',
                'MM_insert_fields' => [
                    'table_local' => 'tx_calendar_exception_group',
                    'tablenames' => 'tx_calendar_exception'
                ]
            ],
        ]
    ]
];
