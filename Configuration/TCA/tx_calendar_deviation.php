<?php
declare(strict_types=1);

if (!defined('TYPO3')) {
    die('Access denied.');
}

return [
    'ctrl' => [
        'title' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_deviation.xlf:tx_calendar_deviation',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'default_sortby' => 'ORDER BY title',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime'
        ],
        'transOrigPointerField' => 'l18n_parent',
        'transOrigDiffSourceField' => 'l18n_diffsource',
        'languageField' => 'sys_language_uid',
        'iconfile' => 'EXT:calendar_foundation/Resources/Public/Icons/tx_calendar_deviation.svg',
        'searchFields' => 'title',
        'hideTable' => true
    ],
    'types' => [
        0 => [
            'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,deviated_event_start,title,teaser,description,start,end,all_day,recurrence,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,hidden,starttime,endtime'
        ]
    ],
    'columns' => [
        'hidden' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'default' => 0
            ]
        ],
        'starttime' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime',
                'default' => 0,
            ]
        ],
        'endtime' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime',
                'default' => 0,
            ]
        ],
        'title' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_deviation.xlf:tx_calendar_deviation.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 128,
                'eval' => 'required'
            ]
        ],
        'teaser' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_deviation.xlf:tx_calendar_deviation.teaser',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
            ]
        ],
        'description' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_deviation.xlf:tx_calendar_deviation.description',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
            ]
        ],
        'start' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_deviation.xlf:tx_calendar_deviation.start',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'dbType' => 'datetime',
                'eval' => 'required,datetime',
                'default' => date('Ymd His')
            ]
        ],
        'end' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_deviation.xlf:tx_calendar_deviation.end',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'dbType' => 'datetime',
                'eval' => 'datetime',
                'default' => date('Ymd His')
            ]
        ],
        'all_day' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_deviation.xlf:tx_calendar_deviation.all_day',
            'config' => [
                'type' => 'check',
                'default' => 0
            ]
        ],
        'recurrence' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_deviation.xlf:tx_calendar_deviation.recurrence',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_calendar_recurrence',
                'foreign_field' => 'entry',
                'appearance' => [
                    'collapseAll' => 1,
                    'expandSingle' => 1,
                ],
                'overrideChildTca' => [
                    'columns' => [
                        'type' => [
                            'config' => [
                                'default' => 'tx_calendar_deviation',
                            ],
                        ],
                    ],
                ],
            ],
        ],
        'deviated_event_start' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_deviation.xlf:tx_calendar_deviation.deviated_event_start',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'dbType' => 'datetime',
                'eval' => 'datetime'
            ]
        ],
        'parent_recurrence_uid' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_deviation.xlf:tx_calendar_deviation.parent_recurrence_uid',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_calendar_recurrence',
                'foreign_field' => 'uid',
            ],
        ],
    ]
];
