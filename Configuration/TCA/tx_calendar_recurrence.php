<?php
declare(strict_types=1);

use JanHelke\CalendarFoundation\Backend\Tca\Label;
use JanHelke\CalendarFoundation\Service\EventService;

if (!defined('TYPO3')) {
    die('Access denied.');
}

$options = [
    'frequency' => [
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.frequency.1',
            EventService::DAILY_RECURRENCE
        ],
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.frequency.2',
            EventService::WEEKLY_RECURRENCE
        ],
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.frequency.3',
            EventService::MONTHLY_RECURRENCE
        ],
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.frequency.4',
            EventService::YEARLY_RECURRENCE
        ]
    ],
    'by_day_of_week' => [
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_of_week.sunday',
            1
        ],
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_of_week.monday',
            2
        ],
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_of_week.tuesday',
            3
        ],
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_of_week.wednesday',
            4
        ],
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_of_week.thursday',
            5
        ],
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_of_week.friday',
            6
        ],
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_of_week.saturday',
            7
        ]
    ],
    'by_day_index' => [
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_index.0',
            0
        ],
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_index.1',
            1
        ],
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_index.2',
            2
        ],
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_index.3',
            3
        ],
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_index.4',
            4
        ],
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_index.5',
            5
        ],
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_index.6',
            6
        ],
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_index.-6',
            -6
        ],
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_index.-5',
            -5
        ],
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_index.-4',
            -4
        ],
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_index.-3',
            -3
        ],
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_index.-2',
            -2
        ],
        [
            'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_index.-1',
            -1
        ],
    ],
    'by_day_of_month' => [
        ['1', ''],
        ['2', ''],
        ['3', ''],
        ['4', ''],
        ['5', ''],
        ['6', ''],
        ['7', ''],
        ['8', ''],
        ['9', ''],
        ['10', ''],
        ['11', ''],
        ['12', ''],
        ['13', ''],
        ['14', ''],
        ['15', ''],
        ['16', ''],
        ['17', ''],
        ['18', ''],
        ['19', ''],
        ['20', ''],
        ['21', ''],
        ['22', ''],
        ['23', ''],
        ['24', ''],
        ['25', ''],
        ['26', ''],
        ['27', ''],
        ['28', ''],
        ['29', ''],
        ['30', ''],
        ['31', ''],
    ]
];

return [
    'ctrl' => [
        'title' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence',
        'label' => 'frequency',
        'label_userFunc' => Label::class . '->getRecurrenceLabel',
        'label_userFunc_options' => $options,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'delete' => 'deleted',
        'type' => 'parent_table',
        'iconfile' => 'EXT:calendar_foundation/Resources/Public/Icons/tx_calendar_recurrence.svg',
        'hideTable' => true
    ],
    'types' => [
        0 => [
            'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,frequency,interval,by_day_of_week,by_day_of_month,by_day_index,by_day_day,until_date,until_recurrence_amount,deviation,exception'
        ],
    ],
    'columns' => [
        'entry' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.entry',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_calendar_entry',
                'foreign_field' => 'uid'
            ]
        ],
        'frequency' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.frequency',
            'onChange' => 'reload',
            'config' => [
                'renderType' => 'selectSingle',
                'type' => 'select',
                'size' => 1,
                'items' => $options['frequency']
            ]
        ],
        'interval' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.interval',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'num',
                'default' => '1'
            ]
        ],
        'by_day_of_week' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_of_week',
            'displayCond' => 'FIELD:frequency:IN:2',
            'config' => [
                'type' => 'check',
                'items' => $options['by_day_of_week'],
                'cols' => 7,
            ],
        ],
        'by_day_of_month' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_of_month',
            'displayCond' => 'FIELD:frequency:IN:3',
            'config' => [
                'type' => 'check',
                'items' => $options['by_day_of_month'],
                'cols' => 10,
            ],
        ],
        'by_day_index' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_index',
            'displayCond' => 'FIELD:frequency:IN:3',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => $options['by_day_index'],
                'default' => 0
            ],
        ],
        'by_day_day' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.by_day_day',
            'displayCond' => 'FIELD:frequency:IN:3',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => array_merge_recursive([['', 0]], $options['by_day_of_week']),
            ],
        ],
        'until_date' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.until_date',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'dbType' => 'datetime',
                'eval' => 'datetime'
            ]
        ],
        'until_recurrence_amount' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.until_recurrence_amount',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'num',
                'default' => 0
            ]
        ],
        'deviation' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.deviation',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_calendar_deviation',
                'foreign_field' => 'parent_recurrence_uid',
                'appearance' => [
                    'collapseAll' => 1,
                    'expandSingle' => 1,
                ],
            ],
        ],
        'exception' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.exception',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'tx_calendar_exception, tx_calendar_exception_group',
                'MM' => 'tx_calendar_mm_relation',
                'MM_insert_fields' => [
                    'table_local' => 'tx_calendar_recurrence'
                ]
            ],
        ],
        'parent_table' => [
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_recurrence.xlf:tx_calendar_recurrence.parent_table',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_entry.xlf:tx_calendar_entry', 'tx_calendar_entry'],
                    ['LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_deviation.xlf:tx_calendar_deviation', 'tx_calendar_deviation'],
                    ['LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_exception.xlf:tx_calendar_exception', 'tx_calendar_exception']
                ],
                'default' => 'tx_calendar_entry'
            ],
        ],
    ]
];
