<?php
declare(strict_types=1);

if (!defined('TYPO3')) {
    die('Access denied.');
}

return [
    'ctrl' => [
        'title' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_calendar.xlf:tx_calendar_calendar',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'default_sortby' => 'ORDER BY title',
        'delete' => 'deleted',
        'type' => 'type',
        'typeicon_column' => 'type',
        'typeicon_classes' => [
            'default' => 'tx-calendar-calendar',
            'calendar' => 'tx-calendar-calendar',
            'feed' => 'tx-calendar-calendar-feed',
            'file' => 'tx-calendar-calendar-file'
        ],
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime'
        ],
        'transOrigPointerField' => 'l18n_parent',
        'transOrigDiffSourceField' => 'l18n_diffsource',
        'languageField' => 'sys_language_uid',
        'iconfile' => 'EXT:calendar_foundation/Resources/Public/Icons/tx_calendar_calendar.svg',
        'searchFields' => 'title'
    ],
    'types' => [
        0 => [
            'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,type,title,activate_free_and_busy,free_and_busy_users_and_groups,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,hidden,starttime,endtime'
        ],
        'calendar' => [
            'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,type,title,activate_free_and_busy,free_and_busy_users_and_groups,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,hidden,starttime,endtime'
        ]
    ],
    'columns' => [
        'hidden' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'default' => 0
            ]
        ],
        'starttime' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime',
                'default' => 0,
            ]
        ],
        'endtime' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime',
                'default' => 0,
            ]
        ],
        'title' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_calendar.xlf:tx_calendar_calendar.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 128,
                'eval' => 'required'
            ]
        ],
        'activate_free_and_busy' => [
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_calendar.xlf:tx_calendar_calendar.activate_free_and_busy',
            'onChange' => 'reload',
            'config' => [
                'type' => 'check',
                'default' => 0,
            ]
        ],
        'free_and_busy_users_and_groups' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_calendar.xlf:tx_calendar_calendar.free_and_busy_users_and_groups',
            'displayCond' => 'FIELD:activate_free_and_busy:=:1',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'MM' => 'tx_calendar_mm_relation',
                'MM_match_fields' => [
                    'table_local' => 'tx_calendar_calendar'
                ],
                'size' => 6,
                'minitems' => 0,
                'maxitems' => 100,
                'allowed' => 'fe_users, fe_groups',
                'default' => 0,
            ]
        ],
        'refresh_interval' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_calendar.xlf:tx_calendar_calendar.refresh_interval',
            'config' => [
                'type' => 'input',
                'size' => 6,
                'max' => 4,
                'eval' => 'num',
                'default' => 60
            ]
        ],
        'scheduler_id' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_calendar.xlf:tx_calendar_calendar.scheduler_id',
            'config' => [
                'type' => 'input',
                'size' => 5,
                'readOnly' => 1,
                'default' => 0,
            ]
        ],
        'md5' => [
            'config' => [
                'type' => 'passthrough'
            ]
        ],
        'type' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_calendar.xlf:tx_calendar_calendar.type',
            'config' => [
                'renderType' => 'selectSingle',
                'type' => 'select',
                'size' => 1,
                'items' => [
                    [
                        'LLL:EXT:calendar_foundation/Resources/Private/Language/locallang_db_calendar.xlf:tx_calendar_calendar.type.I.calendar',
                        'calendar'
                    ]
                ],
                'default' => 'calendar'
            ]
        ],
    ],
];
