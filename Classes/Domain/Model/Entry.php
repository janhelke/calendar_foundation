<?php
declare(strict_types=1);

namespace JanHelke\CalendarFoundation\Domain\Model;

use DateTime;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Entry
 */
class Entry extends AbstractEntity
{
    protected string $title = '';

    protected string $teaser = '';

    protected string $description = '';

    protected \DateTime $start;

    protected \DateTime $end;

    protected bool $allDay = false;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\JanHelke\CalendarFoundation\Domain\Model\Recurrence>|null
     */
    protected ?ObjectStorage $recurrence = null;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getTeaser(): string
    {
        return $this->teaser;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return DateTime
     */
    public function getStart(): DateTime
    {
        return $this->start;
    }

    /**
     * @return DateTime
     */
    public function getEnd(): DateTime
    {
        return $this->end;
    }

    /**
     * @return bool
     */
    public function isAllDay(): bool
    {
        return $this->allDay;
    }

    /**
     * @return ObjectStorage|null
     */
    public function getRecurrence(): ?ObjectStorage
    {
        return $this->recurrence;
    }
}
