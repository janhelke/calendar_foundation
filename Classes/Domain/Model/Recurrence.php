<?php
declare(strict_types=1);

namespace JanHelke\CalendarFoundation\Domain\Model;

use DateTime;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Recurrence
 */
class Recurrence extends AbstractEntity
{
    protected int $entryUid = 0;

    protected int $frequency = 0;

    protected int $interval = 0;

    protected int $byDayOfWeek = 0;

    protected int $byDayOfMonth = 0;

    protected \DateTime $untilDate;

    protected int $untilRecurrenceAmount = 0;

    /**
     * @return int
     */
    public function getEntryUid(): int
    {
        return $this->entryUid;
    }

    /**
     * @return int
     */
    public function getFrequency(): int
    {
        return $this->frequency;
    }

    /**
     * @return int
     */
    public function getInterval(): int
    {
        return $this->interval;
    }

    /**
     * @return int
     */
    public function getByDayOfWeek(): int
    {
        return $this->byDayOfWeek;
    }

    /**
     * @return int
     */
    public function getByDayOfMonth(): int
    {
        return $this->byDayOfMonth;
    }

    /**
     * @return DateTime
     */
    public function getUntilDate(): DateTime
    {
        return $this->untilDate;
    }

    /**
     * @return int
     */
    public function getUntilRecurrenceAmount(): int
    {
        return $this->untilRecurrenceAmount;
    }
}
