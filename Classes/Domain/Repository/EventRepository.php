<?php
declare(strict_types=1);

namespace JanHelke\CalendarFoundation\Domain\Repository;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Event Repository
 */
class EventRepository
{

    /**
     * @param array $entries
     * @return int
     */
    public function insertMultipleEventEntries(array $entries): int
    {
        return GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_calendar_event')
            ->bulkInsert(
                'tx_calendar_event',
                $entries,
                [
                    'start',
                    'end',
                    'original_entry_uid',
                    'entry_uid'
                ]
            );
    }

    /**
     * @param int ...$eventUids
     */
    public function removeEventsFromTableByEntryUids(int ...$eventUids): void
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_calendar_event');
        $queryBuilder
            ->delete('tx_calendar_event')
            ->where(
                $queryBuilder->expr()->in(
                    'original_entry_uid',
                    $eventUids
                )
            )
            ->execute();
    }
}
