<?php
declare(strict_types=1);

namespace JanHelke\CalendarFoundation\Domain\Repository;

use PDO;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Recurrence Repository
 */
class RecurrenceRepository
{

    /**
     * @param int $uid
     * @return array
     */
    public function findByEntryUid(int $uid): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_calendar_recurrence');
        $deviationRepository = new DeviationRepository();
        $exceptionRepository = new ExceptionRepository();

        $recurrenceArray = $queryBuilder
            ->select('*')
            ->from('tx_calendar_recurrence')
            ->where(
                $queryBuilder->expr()->eq(
                    'entry',
                    $queryBuilder->createNamedParameter($uid, PDO::PARAM_INT)
                ),
                $queryBuilder->expr()->eq(
                    'parent_table',
                    $queryBuilder->createNamedParameter('tx_calendar_entry')
                )
            )
            ->execute()
            ->fetchAll();

        foreach ($recurrenceArray as $key => $recurrence) {
            if (!empty($recurrence['deviation']) && $recurrence['deviation'] > 0) {
                $recurrenceArray[$key]['deviation'] = $deviationRepository->findByRecurrenceUid($recurrence['uid']);
            } else {
                unset($recurrenceArray[$key]['deviation']);
            }

            if (!empty($recurrence['exception']) && $recurrence['exception'] > 0) {
                $recurrenceArray[$key]['exception'] = $exceptionRepository->findByRecurrenceUid($recurrence['uid']);
            } else {
                unset($recurrenceArray[$key]['exception']);
            }
        }

        return $recurrenceArray;
    }
}
