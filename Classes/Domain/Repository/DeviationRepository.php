<?php
declare(strict_types=1);

namespace JanHelke\CalendarFoundation\Domain\Repository;

use PDO;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Deviation Repository
 */
class DeviationRepository
{
    /**
     * @return array
     */
    public function findAll(): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_calendar_deviation');
        return $queryBuilder
            ->select('*')
            ->from('tx_calendar_deviation')
            ->execute()
            ->fetchAll() ?: [];
    }

    /**
     * @param int $uid
     * @return array
     */
    public function findByUid(int $uid): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_calendar_deviation');
        return $queryBuilder
            ->select('*')
            ->from('tx_calendar_deviation')
            ->where(
                $queryBuilder->expr()->eq(
                    'uid',
                    $queryBuilder->createNamedParameter($uid, PDO::PARAM_INT)
                )
            )
            ->execute()
            ->fetch() ?: [];
    }

    /**
     * @param int $recurrenceUid
     * @return array
     */
    public function findByRecurrenceUid(int $recurrenceUid): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_calendar_deviation');
        return $queryBuilder
            ->select('*')
            ->from('tx_calendar_deviation')
            ->where(
                $queryBuilder->expr()->eq(
                    'parent_recurrence_uid',
                    $queryBuilder->createNamedParameter($recurrenceUid, PDO::PARAM_INT)
                )
            )
            ->execute()
            ->fetchAll() ?: [];
    }
}
