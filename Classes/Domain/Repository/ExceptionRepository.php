<?php
declare(strict_types=1);

namespace JanHelke\CalendarFoundation\Domain\Repository;

use PDO;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Exception Repository
 */
class ExceptionRepository
{
    /**
     * @return array
     */
    public function findAll(): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_calendar_exception');
        return $queryBuilder
            ->select('*')
            ->from('tx_calendar_exception')
            ->execute()
            ->fetchAll() ?: [];
    }

    /**
     * @param int $uid
     * @return array
     */
    public function findByUid(int $uid): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_calendar_exception');
        return $queryBuilder
            ->select('*')
            ->from('tx_calendar_exception')
            ->where(
                $queryBuilder->expr()->eq(
                    'uid',
                    $queryBuilder->createNamedParameter($uid, PDO::PARAM_INT)
                )
            )
            ->execute()
            ->fetch() ?: [];
    }

    /**
     * This function returns the exceptions configured for the given recurrence UID as well as the exceptions
     * associated with possible exception groups.
     *
     * @param int $recurrenceUid
     * @return array
     */
    public function findByRecurrenceUid(int $recurrenceUid): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_calendar_exception');
        $exceptions = $queryBuilder
            ->select('*')
            ->from('tx_calendar_exception', 'exception')
            ->join('exception', 'tx_calendar_mm_relation', 'mm', 'mm.uid_foreign = exception.uid')
            ->where(
                $queryBuilder->expr()->eq(
                    'mm.uid_local',
                    $queryBuilder->createNamedParameter($recurrenceUid, PDO::PARAM_INT)
                ),
                $queryBuilder->expr()->eq(
                    'mm.table_local',
                    $queryBuilder->createNamedParameter('tx_calendar_recurrence')
                ),
                $queryBuilder->expr()->eq(
                    'mm.tablenames',
                    $queryBuilder->createNamedParameter('tx_calendar_exception')
                )
            )
            ->execute()
            ->fetchAll() ?: [];

        $exceptionsFromGroup = $this->findExceptionsFromGroupsByRecurrenceUid($recurrenceUid);

        return array_merge($exceptions, $exceptionsFromGroup);
    }

    /**
     * This function returns all exceptions associated with exception groups for a given recurrence UID.
     *
     * @param int $recurrenceUid
     * @return array
     */
    protected function findExceptionsFromGroupsByRecurrenceUid(int $recurrenceUid): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_calendar_exception_group');
        $exceptionGroups = $queryBuilder
            ->select('*')
            ->from('tx_calendar_exception_group', 'exception_group')
            ->join('exception_group', 'tx_calendar_mm_relation', 'mm', 'mm.uid_foreign = exception_group.uid')
            ->where(
                $queryBuilder->expr()->eq(
                    'mm.uid_local',
                    $queryBuilder->createNamedParameter($recurrenceUid, PDO::PARAM_INT)
                ),
                $queryBuilder->expr()->eq(
                    'mm.table_local',
                    $queryBuilder->createNamedParameter('tx_calendar_recurrence')
                ),
                $queryBuilder->expr()->eq(
                    'mm.tablenames',
                    $queryBuilder->createNamedParameter('tx_calendar_exception_group')
                )
            )->execute()
            ->fetchAll() ?: [];

        $exceptionsFromGroup = [];
        if (!empty($exceptionGroups)) {
            foreach ($exceptionGroups as $exceptionGroup) {
                $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_calendar_exception');
                $exceptionsFromGroup = $queryBuilder
                    ->select('*')
                    ->from('tx_calendar_exception', 'exception')
                    ->join('exception', 'tx_calendar_mm_relation', 'mm', 'mm.uid_foreign = exception.uid')
                    ->where(
                        $queryBuilder->expr()->eq(
                            'mm.uid_local',
                            $queryBuilder->createNamedParameter($exceptionGroup['uid_foreign'], PDO::PARAM_INT)
                        ),
                        $queryBuilder->expr()->eq(
                            'mm.table_local',
                            $queryBuilder->createNamedParameter('tx_calendar_exception_group')
                        ),
                        $queryBuilder->expr()->eq(
                            'mm.tablenames',
                            $queryBuilder->createNamedParameter('tx_calendar_exception')
                        )
                    )
                    ->execute()
                    ->fetchAll() ?: [];
            }
        }

        return $exceptionsFromGroup;
    }
}
