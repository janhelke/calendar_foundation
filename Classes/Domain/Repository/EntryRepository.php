<?php
declare(strict_types=1);

namespace JanHelke\CalendarFoundation\Domain\Repository;

use PDO;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Entry Repository
 */
class EntryRepository
{
    /**
     * @return array
     */
    public function findAll(): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_calendar_entry');
        return $queryBuilder
            ->select('*')
            ->from('tx_calendar_entry')
            ->execute()
            ->fetchAll() ?: [];
    }

    /**
     * @param int $uid
     * @return array
     */
    public function findByUid(int $uid): array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_calendar_entry');
        return $queryBuilder
            ->select('*')
            ->from('tx_calendar_entry')
            ->where(
                $queryBuilder->expr()->eq(
                    'uid',
                    $queryBuilder->createNamedParameter($uid, PDO::PARAM_INT)
                )
            )
            ->execute()
            ->fetch() ?: [];
    }
}
