<?php
declare(strict_types=1);

namespace JanHelke\CalendarFoundation\Backend\Tca;

/**
 * Class Label
 */
class Label
{
    /**
     * @param array $parameters
     */
    public function getRecurrenceLabel(array &$parameters): void
    {
        if (isset($parameters['table'], $parameters['row']['frequency'][0]) && $parameters['table'] === 'tx_calendar_recurrence') {
            $label = '';
            switch ($parameters['row']['frequency'][0]) {
                case 1:
                    $label = $this->getRecurrenceLabelForDaily($parameters);
                    break;
                case 2:
                    $label = $this->getRecurrenceLabelForWeekly($parameters);
                    break;
                case 3:
                    $label = $this->getRecurrenceLabelForMonthly($parameters);
                    break;
                case 4:
                    $label = $this->getRecurrenceLabelForYearly($parameters);
                    break;
            }

            if (!empty($parameters['row']['exception']) && !empty($parameters['row']['deviation'])) {
                $label .= ' (with exception and deviation)';
            } elseif (!empty($parameters['row']['deviation'])) {
                $label .= ' (with deviation)';
            } elseif (!empty($parameters['row']['exception'])) {
                $label .= ' (with exception)';
            }

            $parameters['title'] = $label;
        }
    }

    /**
     * @param array $parameters
     * @return string
     */
    protected function getRecurrenceLabelForDaily(array $parameters): string
    {
        return $parameters['row']['interval'] === 1 ? 'Daily, every day' : 'Daily, every ' . $parameters['row']['interval'] . ' days';
    }

    /**
     * @param array $parameters
     * @return string
     */
    protected function getRecurrenceLabelForWeekly(array $parameters): string
    {
        if (empty($parameters['row']['by_day_of_week'])) {
            $label = 'Weekly, invalid selection';
        } else {
            $elements = $this->getSelectedOptions(
                $parameters['row']['by_day_of_week'],
                $parameters['options']['by_day_of_week'] ?? []
            );
            $label = 'Weekly, every ' . ($parameters['row']['interval'] > 1 ? $parameters['row']['interval'] . ' ' : '')
                . implode(
                    ', ',
                    $elements
                );
        }

        return $label;
    }

    /**
     * @param array $parameters
     * @return string
     */
    protected function getRecurrenceLabelForMonthly(array $parameters): string
    {
        if (!empty($parameters['row']['by_day_index']) && !empty($parameters['row']['by_day_day'])) {
            foreach ($parameters['options']['by_day_index'] as $value) {
                if ($value[1] === $parameters['row']['by_day_index']) {
                    $by_day_index = $GLOBALS['LANG']->sL($value[0]);
                }
            }

            foreach ($parameters['options']['by_day_of_week'] as $value) {
                if ($value[1] === $parameters['row']['by_day_day']) {
                    $by_day_day = $GLOBALS['LANG']->sL($value[0]);
                }
            }

            if (!empty($by_day_index) && !empty($by_day_day)) {
                $label = 'Monthly, every ' . $by_day_index . ' ' . $by_day_day;
            } else {
                $label = 'Monthly, invalid selection';
            }
        } elseif (!empty($parameters['row']['by_day_of_month'])) {
            $elements = $this->getSelectedOptions(
                $parameters['row']['by_day_of_month'],
                $parameters['options']['by_day_of_month'] ?? []
            );
            $label = 'Monthly, every ' . ($parameters['row']['interval'] > 1 ? $parameters['row']['interval'] . ' ' : '')
                . implode(
                    ', ',
                    $elements
                );
        } else {
            $label = 'Monthly, invalid selection';
        }

        return $label;
    }

    /**
     * @param array $parameters
     * @return string
     */
    protected function getRecurrenceLabelForYearly(array $parameters): string
    {
        return 'Yearly' . ($parameters['row']['interval'] > 1 ? ', every ' . $parameters['row']['interval'] : '');
    }

    /**
     * @param int $formElementValue
     * @param array $options
     * @return array
     */
    protected function getSelectedOptions(int $formElementValue, array $options): array
    {
        $returnArray = [];
        foreach ($options as $key => $value) {
            $checkboxPow = 2 ** $key;
            if (($formElementValue & $checkboxPow) !== 0) {
                $returnArray[] = $GLOBALS['LANG']->sL($value[0]);
            }
        }

        return $returnArray;
    }
}
