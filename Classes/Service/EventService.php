<?php
declare(strict_types=1);

namespace JanHelke\CalendarFoundation\Service;

use DateInterval;
use DateTime;
use Exception;
use JanHelke\CalendarFoundation\Domain\Repository\EntryRepository;
use JanHelke\CalendarFoundation\Domain\Repository\EventRepository;
use JanHelke\CalendarFoundation\Domain\Repository\RecurrenceRepository;
use JanHelke\CalendarFoundation\Utility\CalculateDateTimeUtility;

/**
 * Event service
 */
class EventService
{
    /**
     * @var int
     */
    public const DAILY_RECURRENCE = 1;

    /**
     * @var int
     */
    public const WEEKLY_RECURRENCE = 2;

    /**
     * @var int
     */
    public const MONTHLY_RECURRENCE = 3;

    /**
     * @var int
     */
    public const YEARLY_RECURRENCE = 4;

    protected ?\DateTime $untilDate = null;

    /**
     * EventService constructor.
     * @throws Exception
     */
    public function __construct()
    {
        // As we don't want to overload the server upon calculating future date
        // we set the maximal untilDate to five years into the future.
        // @todo Make that configurable in the extension settings.
        $this->setUntilDateBasedOnYears(5);
    }

    /**
     * @param array $entryUids
     * @return int
     */
    public function createAndSaveEventIndex(array $entryUids = []): int
    {
        $entryRepository = new EntryRepository();
        $events = 0;
        if (empty($entryUids)) {
            $entryUids = [];
            foreach ($entryRepository->findAll() as $entry) {
                $entryUids[] = $entry['uid'];
            }
        }

        foreach ($entryUids as $entryUid) {
            $events += $this->createAndSaveEventIndexForEntryUid($entryUid);
        }

        return $events;
    }

    /**
     * @param int $entryUid
     * @return int
     */
    public function createAndSaveEventIndexForEntryUid(int $entryUid): int
    {
        $events = [];
        $amount = 0;
        $entryRepository = new EntryRepository();
        $recurrenceRepository = new RecurrenceRepository();
        $eventRepository = new EventRepository();

        $entry = $entryRepository->findByUid($entryUid);

        if (!empty($entry)) {
            $recurrences = $recurrenceRepository->findByEntryUid($entry['uid']);
            if (!empty($recurrences)) {
                foreach ($recurrences as $recurrence) {
                    $eventsPerRecurrence = $this->calculateEventRows(
                        $entry,
                        $recurrence
                    );
                    foreach ($eventsPerRecurrence as $event) {
                        $events[] = $event;
                    }

                    $amount += count($events);
                }
            } else {
                $events = $this->calculateEventRows(
                    $entry
                );
                $amount += count($events);
            }
        }

        if (!empty($events)) {
            $eventRepository->removeEventsFromTableByEntryUids($entryUid);
            $eventRepository->insertMultipleEventEntries($events);
        }

        return $amount;
    }

    /**
     * Calculate all events for a single entry and all its recurrences.
     *
     * @param array $calendarEntry
     * @param array $recurrenceDatabaseRow
     * @return array
     */
    public function calculateEventRows(array $calendarEntry, array $recurrenceDatabaseRow = []): array
    {
        $eventArray = [];

        if (!empty($recurrenceDatabaseRow['until_date']) && $recurrenceDatabaseRow['until_date'] > 0
            // This is a workaround to make the functional tests run. For some reasons unknown we are not able to insert
            // '0000-00-00 00:00:00' as a NULL value.
            && $recurrenceDatabaseRow['until_date'] !== '1000-01-01 00:00:00') {
            $this->untilDate = new DateTime($recurrenceDatabaseRow['until_date']);
        }

        if ($calendarEntry['all_day'] === 1) {
            $calendarEntry['start'] = CalculateDateTimeUtility::calculateStartOfDay(new DateTime($calendarEntry['start']))->format('Y-m-d H:i:s');
            $calendarEntry['end'] = CalculateDateTimeUtility::calculateEndOfDay(new DateTime($calendarEntry['end']))->format('Y-m-d H:i:s');
        }

        if (!empty($recurrenceDatabaseRow)) {
            switch ($recurrenceDatabaseRow['frequency']) {
                case self::DAILY_RECURRENCE:
                    $this->calculateDailyRecurrences($eventArray, $calendarEntry, $recurrenceDatabaseRow);
                    break;
                case self::WEEKLY_RECURRENCE:
                    $this->calculateWeeklyRecurrences($eventArray, $calendarEntry, $recurrenceDatabaseRow);
                    break;
                case self::MONTHLY_RECURRENCE:
                    if (!empty($recurrenceDatabaseRow['by_day_index']) && !empty($recurrenceDatabaseRow['by_day_day'])) {
                        $this->calculateMonthlyRecurrenceByDay($eventArray, $calendarEntry, $recurrenceDatabaseRow);
                    } else {
                        $this->calculateMonthlyRecurrences($eventArray, $calendarEntry, $recurrenceDatabaseRow);
                    }

                    break;
                case self::YEARLY_RECURRENCE:
                    $this->calculateYearlyRecurrences($eventArray, $calendarEntry, $recurrenceDatabaseRow);
                    break;
                default:
                    $this->calculateSingleRecurrence($eventArray, $calendarEntry);
            }

            if (!empty($recurrenceDatabaseRow['deviation'])) {
                $this->applyDeviations($eventArray, $recurrenceDatabaseRow['deviation']);
            }

            if (!empty($recurrenceDatabaseRow['exception'])) {
                $this->removeExceptions($eventArray, $recurrenceDatabaseRow['exception']);
            }
        } else {
            $this->calculateSingleRecurrence($eventArray, $calendarEntry);
        }

        return $eventArray;
    }

    /**
     * @param array $eventArray
     * @param array $calendarEntry
     */
    protected function calculateSingleRecurrence(
        array &$eventArray,
        array $calendarEntry
    ): void {
        $eventArray = [
            [
                'start' => $calendarEntry['start'],
                'end' => $calendarEntry['end'],
                'original_entry_uid' => $calendarEntry['uid'],
                'entry_uid' => $calendarEntry['uid'],
            ]
        ];
    }

    /**
     * @param array $eventArray
     * @param array $calendarEntry
     * @param array $recurrenceDatabaseRow
     * @throws Exception
     */
    protected function calculateDailyRecurrences(
        array &$eventArray,
        array $calendarEntry,
        array $recurrenceDatabaseRow
    ): void {
        $counter = 1;
        $eventArray = [
            [
                'start' => $calendarEntry['start'],
                'end' => $calendarEntry['end'],
                'original_entry_uid' => $calendarEntry['uid'],
                'entry_uid' => $calendarEntry['uid'],
            ]
        ];
        $nextStartDate = (new DateTime($calendarEntry['start']))->add(new DateInterval('P' . ($counter * $recurrenceDatabaseRow['interval']) . 'D'));

        while (
            $this->recurrenceAmountIsNotReached($counter, $recurrenceDatabaseRow['until_recurrence_amount'])
            && $this->recurrenceEndDateIsNotReached($nextStartDate, $this->untilDate)
        ) {
            $eventArray[] = [
                'start' => $nextStartDate->format('Y-m-d H:i:s'),
                'end' => (new DateTime($calendarEntry['end']))->add(new DateInterval('P' . ($counter * $recurrenceDatabaseRow['interval']) . 'D'))->format('Y-m-d H:i:s'),
                'original_entry_uid' => $calendarEntry['uid'],
                'entry_uid' => $calendarEntry['uid'],
            ];

            ++$counter;
            $nextStartDate = (new DateTime($calendarEntry['start']))->add(new DateInterval('P' . ($counter * $recurrenceDatabaseRow['interval']) . 'D'));
        }
    }

    /**
     * @param array $eventArray
     * @param array $calendarEntry
     * @param array $recurrenceDatabaseRow
     */
    protected function calculateWeeklyRecurrences(
        array &$eventArray,
        array $calendarEntry,
        array $recurrenceDatabaseRow
    ): void {
        $counter = 1;
        $startDate = new DateTime($calendarEntry['start']);
        $endDate = new DateTime($calendarEntry['end']);

        if (empty($daysOfTheWeek = $this->getValuesFromCheckboxes($recurrenceDatabaseRow['by_day_of_week']))) {
            // In case no day of the week was selected, we assume, that the event should happen on the same day as
            // the start date.
            $daysOfTheWeek = [(int)$startDate->format('w') + 1];
        }

        if (!in_array((int)$startDate->format('w') + 1, $daysOfTheWeek, true)) {
            // We expect that the user wants to occur the first event ASAP and not only after the interval.
            $difference = $this->findNextWeeklyDifference(clone $startDate, $daysOfTheWeek, 1);
            $startDate->add(new DateInterval('P' . $difference . 'D'));
            $endDate->add(new DateInterval('P' . $difference . 'D'));
        }

        $eventArray = [
            [
                'start' => $startDate->format('Y-m-d H:i:s'),
                'end' => $endDate->format('Y-m-d H:i:s'),
                'original_entry_uid' => $calendarEntry['uid'],
                'entry_uid' => $calendarEntry['uid'],
            ]
        ];
        $difference = $this->findNextWeeklyDifference(
            clone $startDate,
            $daysOfTheWeek,
            $recurrenceDatabaseRow['interval']
        );
        $startDate->add(new DateInterval('P' . $difference . 'D'));
        $endDate->add(new DateInterval('P' . $difference . 'D'));

        while (
            $this->recurrenceAmountIsNotReached($counter, $recurrenceDatabaseRow['until_recurrence_amount'])
            && $startDate <= $this->untilDate
        ) {
            $eventArray[] = [
                'start' => $startDate->format('Y-m-d H:i:s'),
                'end' => $endDate->format('Y-m-d H:i:s'),
                'original_entry_uid' => $calendarEntry['uid'],
                'entry_uid' => $calendarEntry['uid'],
            ];
            $difference = $this->findNextWeeklyDifference(
                clone $startDate,
                $daysOfTheWeek,
                $recurrenceDatabaseRow['interval']
            );
            $startDate->add(new DateInterval('P' . $difference . 'D'));
            $endDate->add(new DateInterval('P' . $difference . 'D'));

            ++$counter;
        }
    }

    /**
     * @param array $eventArray
     * @param array $calendarEntry
     * @param array $recurrenceDatabaseRow
     */
    protected function calculateMonthlyRecurrences(
        array &$eventArray,
        array $calendarEntry,
        array $recurrenceDatabaseRow
    ): void {
        $counter = 1;
        $startDate = new DateTime($calendarEntry['start']);
        $endDate = new DateTime($calendarEntry['end']);

        if (empty($daysOfTheMonth = $this->getValuesFromCheckboxes($recurrenceDatabaseRow['by_day_of_month']))) {
            // In case no day of the month was selected, we assume, that the event should happen on the same day as
            // the start date.
            $daysOfTheMonth = [(int)$startDate->format('j')];
        }

        if (!in_array((int)$startDate->format('j'), $daysOfTheMonth, true)) {
            // We expect that the user wants to occur the first event ASAP and not only after the interval.
            $difference = $this->findNextMonthlyDifference(clone $startDate, $daysOfTheMonth, 1);
            $startDate->add(new DateInterval('P' . $difference . 'D'));
            $endDate->add(new DateInterval('P' . $difference . 'D'));
        }

        $eventArray = [
            [
                'start' => $startDate->format('Y-m-d H:i:s'),
                'end' => $endDate->format('Y-m-d H:i:s'),
                'original_entry_uid' => $calendarEntry['uid'],
                'entry_uid' => $calendarEntry['uid'],
            ]
        ];
        $difference = $this->findNextMonthlyDifference(
            clone $startDate,
            $daysOfTheMonth,
            $recurrenceDatabaseRow['interval']
        );
        $startDate->add(new DateInterval('P' . $difference . 'D'));
        $endDate->add(new DateInterval('P' . $difference . 'D'));

        while (
            $this->recurrenceAmountIsNotReached($counter, $recurrenceDatabaseRow['until_recurrence_amount'])
            && $startDate <= $this->untilDate
        ) {
            $eventArray[] = [
                'start' => $startDate->format('Y-m-d H:i:s'),
                'end' => $endDate->format('Y-m-d H:i:s'),
                'original_entry_uid' => $calendarEntry['uid'],
                'entry_uid' => $calendarEntry['uid'],
            ];
            $difference = $this->findNextMonthlyDifference(
                clone $startDate,
                $daysOfTheMonth,
                $recurrenceDatabaseRow['interval']
            );
            $startDate->add(new DateInterval('P' . $difference . 'D'));
            $endDate->add(new DateInterval('P' . $difference . 'D'));

            ++$counter;
        }
    }

    /**
     * This function calculates recurrences like "Every second Friday of the Month"
     *
     * @param array $eventArray
     * @param array $calendarEntry
     * @param array $recurrenceDatabaseRow
     */
    protected function calculateMonthlyRecurrenceByDay(
        array &$eventArray,
        array $calendarEntry,
        array $recurrenceDatabaseRow
    ): void {
        $counter = 1;
        $startDate = new DateTime($calendarEntry['start']);
        $endDate = new DateTime($calendarEntry['end']);

        // The difference between startDate and endDate
        $duration = $startDate->diff($endDate);

        $calculatedStartDate = $this->calculateStartDateForMonthlyRecurrenceByDay($recurrenceDatabaseRow, $startDate, $counter);

        if ($calculatedStartDate > $startDate) {
            $calculatedEndDate = clone $calculatedStartDate;
            $calculatedEndDate = $calculatedEndDate->add($duration);
            $eventArray = [
                [
                    'start' => $calculatedStartDate->format('Y-m-d H:i:s'),
                    'end' => $calculatedEndDate->format('Y-m-d H:i:s'),
                    'original_entry_uid' => $calendarEntry['uid'],
                    'entry_uid' => $calendarEntry['uid'],
                ]
            ];
        }

        ++$counter;

        while (
            $this->recurrenceAmountIsNotReached($counter, $recurrenceDatabaseRow['until_recurrence_amount'])
            && $calculatedStartDate <= $this->untilDate
        ) {
            $calculatedStartDate = $this->calculateStartDateForMonthlyRecurrenceByDay($recurrenceDatabaseRow, $startDate, $counter);
            if ($calculatedStartDate <= $this->untilDate) {
                $calculatedEndDate = clone $calculatedStartDate;
                $calculatedEndDate = $calculatedEndDate->add($duration);

                $eventArray[] = [
                    'start' => $calculatedStartDate->format('Y-m-d H:i:s'),
                    'end' => $calculatedEndDate->format('Y-m-d H:i:s'),
                    'original_entry_uid' => $calendarEntry['uid'],
                    'entry_uid' => $calendarEntry['uid'],
                ];
                ++$counter;
            }
        }
    }

    /**
     * @param array $recurrenceDatabaseRow
     * @param DateTime $startDate
     * @param int $counter
     * @return DateTime
     * @throws Exception
     */
    protected function calculateStartDateForMonthlyRecurrenceByDay(
        array $recurrenceDatabaseRow,
        DateTime $startDate,
        int $counter
    ): DateTime {
        // The time, the event starts
        $startTime = (new DateTime($startDate->format('Y-m-d')))->diff($startDate);

        $dayOfWeekMap = [
            1 => 'Sunday',
            2 => 'Monday',
            3 => 'Tuesday',
            4 => 'Wednesday',
            5 => 'Thursday',
            6 => 'Friday',
            7 => 'Saturday'
        ];

        $month = (int)$startDate->format('m') + $counter - 1;
        $year = (int)$startDate->format('Y');

        while ($month > 12) {
            $month -= 12;
            ++$year;
        }

        if ($month === 0) {
            $month = 1;
        }

        if (abs((int)$recurrenceDatabaseRow['by_day_index']) <= 5) {
            $interval = new DateInterval('P' . (abs((int)$recurrenceDatabaseRow['by_day_index']) - 1) . 'W');
        } else {
            $interval = new DateInterval('P' . (5 - 1) . 'W');
        }

        if ($recurrenceDatabaseRow['by_day_index'] > 0) {
            $calculatedStartDate = new DateTime('First ' . $dayOfWeekMap[$recurrenceDatabaseRow['by_day_day']] . ' of ' . $year . '-' . str_pad(
                (string)$month,
                2,
                '0',
                STR_PAD_LEFT
            ));
            $calculatedStartDate->add($interval);
            if (
                ($recurrenceDatabaseRow['by_day_index'] === 6 && $month < (int)$calculatedStartDate->format('m'))
                || ((int)$calculatedStartDate->format('m') === 1 && $month > (int)$calculatedStartDate->format('m'))
            ) {
                $calculatedStartDate->sub(new DateInterval('P1W'));
            }
        } else {
            $calculatedStartDate = new DateTime('Last ' . $dayOfWeekMap[$recurrenceDatabaseRow['by_day_day']] . ' of ' . $year . '-' . str_pad(
                (string)$month,
                2,
                '0',
                STR_PAD_LEFT
            ));
            $calculatedStartDate->sub($interval);
            if ($recurrenceDatabaseRow['by_day_index'] === -6 && $month > (int)$calculatedStartDate->format('m')) {
                $calculatedStartDate->add(new DateInterval('P1W'));
            }
        }

        $calculatedStartDate->add($startTime);
        return $calculatedStartDate;
    }

    /**
     * @param array $eventArray
     * @param array $calendarEntry
     * @param array $recurrenceDatabaseRow
     */
    protected function calculateYearlyRecurrences(
        array &$eventArray,
        array $calendarEntry,
        array $recurrenceDatabaseRow
    ): void {
        $counter = 1;
        $startDate = new DateTime($calendarEntry['start']);
        $endDate = new DateTime($calendarEntry['end']);

        $eventArray = [
            [
                'start' => $startDate->format('Y-m-d H:i:s'),
                'end' => $endDate->format('Y-m-d H:i:s'),
                'original_entry_uid' => $calendarEntry['uid'],
                'entry_uid' => $calendarEntry['uid'],
            ]
        ];
        $startDate->add(new DateInterval('P' . $recurrenceDatabaseRow['interval'] . 'Y'));
        $endDate->add(new DateInterval('P' . $recurrenceDatabaseRow['interval'] . 'Y'));
        while (
            $this->recurrenceAmountIsNotReached($counter, $recurrenceDatabaseRow['until_recurrence_amount'])
            && $startDate <= $this->untilDate
        ) {
            $eventArray[] = [
                'start' => $startDate->format('Y-m-d H:i:s'),
                'end' => $endDate->format('Y-m-d H:i:s'),
                'original_entry_uid' => $calendarEntry['uid'],
                'entry_uid' => $calendarEntry['uid'],
            ];
            $startDate->add(new DateInterval('P' . $recurrenceDatabaseRow['interval'] . 'Y'));
            $endDate->add(new DateInterval('P' . $recurrenceDatabaseRow['interval'] . 'Y'));

            ++$counter;
        }
    }

    /**
     * @param DateTime $lastRecurrences
     * @param array $daysOfWeek
     * @param int $interval
     * @return int
     */
    protected function findNextWeeklyDifference(DateTime $lastRecurrences, array $daysOfWeek, int $interval): int
    {
        $lastDayOfTheWeek = (int)$lastRecurrences->format('w') + 1;
        if (!in_array($lastDayOfTheWeek, $daysOfWeek, true)) {
            foreach ($daysOfWeek as $key => $day) {
                if ($lastDayOfTheWeek > $day
                    && isset($daysOfWeek[$key + 1])
                    && $lastDayOfTheWeek < $daysOfWeek[$key + 1]
                ) {
                    $nextDayOfTheWeek = $daysOfWeek[$key + 1];
                }
            }

            $nextDayOfTheWeek ??= $daysOfWeek[0];
        } else {
            $key = array_search($lastDayOfTheWeek, $daysOfWeek, true);
            $nextDayOfTheWeek = $daysOfWeek[$key + 1] ?? $daysOfWeek[0];
        }

        $difference = $nextDayOfTheWeek - $lastDayOfTheWeek;

        return $difference <= 0 ? $difference + ($interval * 7) : $difference;
    }

    /**
     * @param DateTime $lastRecurrences
     * @param array $daysOfMonth
     * @param int $interval
     * @return int
     */
    protected function findNextMonthlyDifference(DateTime $lastRecurrences, array $daysOfMonth, int $interval): int
    {
        $lastDayOfTheMonth = (int)$lastRecurrences->format('j');
        if (!in_array($lastDayOfTheMonth, $daysOfMonth, true)) {
            foreach ($daysOfMonth as $key => $day) {
                if ($lastDayOfTheMonth > $day
                    && isset($daysOfMonth[$key + 1])
                    && $lastDayOfTheMonth < $daysOfMonth[$key + 1]
                ) {
                    $nextDayOfTheMonth = $daysOfMonth[$key + 1];
                }
            }

            $nextDayOfTheMonth ??= $daysOfMonth[0];
        } else {
            $key = array_search($lastDayOfTheMonth, $daysOfMonth, true);
            $nextDayOfTheMonth = $daysOfMonth[$key + 1] ?? $daysOfMonth[0];
        }

        $difference = $nextDayOfTheMonth - $lastDayOfTheMonth;
        // If the difference is negative, if means we need to switch to the next month
        if ($difference <= 0) {
            for ($i = 0; $i < $interval; ++$i) {
                $difference += (int)$lastRecurrences->format('t');
                $lastRecurrences->add(new DateInterval('P1M'));
            }
        }

        return $difference;
    }

    /**
     * In this function we remove every event, that intersects with the given exception range.
     *
     * @param array $eventArray
     * @param array $exceptionArray
     */
    protected function removeExceptions(
        array &$eventArray,
        array $exceptionArray
    ): void {
        foreach ($exceptionArray as $exception) {
            if (!empty($exception['all_day']) && $exception['all_day'] === 1) {
                $exceptionStart = CalculateDateTimeUtility::calculateStartOfDay(new DateTime($exception['start']));
                $exceptionEnd = CalculateDateTimeUtility::calculateEndOfDay(new DateTime($exception['end']));
            } else {
                $exceptionStart = new DateTime($exception['start']);
                $exceptionEnd = new DateTime($exception['end']);
            }

            foreach ($eventArray as $key => $event) {
                $eventStart = new DateTime($event['start']);
                $eventEnd = new DateTime($event['end']);
                if (
                    (
                        ($eventStart <= $exceptionStart && $exceptionStart <= $eventEnd && $eventEnd <= $exceptionEnd) ||
                        ($exceptionStart <= $eventStart && $eventStart <= $exceptionEnd && $exceptionEnd <= $eventEnd) ||
                        ($eventStart <= $exceptionStart && $exceptionEnd <= $eventEnd) ||
                        ($exceptionStart <= $eventStart && $eventEnd <= $exceptionEnd)
                    ) &&
                    // This line ensures, that deviations will be kept, even if there is an exception during this time.
                    ($event['entry_uid'] === $event['original_entry_uid'])

                ) {
                    unset($eventArray[$key]);
                }
            }
        }

        $eventArray = array_merge($eventArray);
    }

    /**
     * In this function we apply all deviations.
     *
     * @param array $eventArray
     * @param array $deviationArray
     */
    protected function applyDeviations(
        array &$eventArray,
        array $deviationArray
    ): void {
        foreach ($deviationArray as $deviation) {
            $deviatedEventStart = new DateTime($deviation['deviated_event_start']);
            foreach ($eventArray as $key => $event) {
                $eventStart = new DateTime($event['start']);
                if ($eventStart->format('Y-m-d H:i:s') === $deviatedEventStart->format('Y-m-d H:i:s')) {
                    if (!empty($deviation['all_day']) && $deviation['all_day'] === 1) {
                        $eventArray[$key]['start'] = CalculateDateTimeUtility::calculateStartOfDay(new DateTime($deviation['start']))->format('Y-m-d H:i:s');
                        $eventArray[$key]['end'] = CalculateDateTimeUtility::calculateEndOfDay(new DateTime($deviation['end']))->format('Y-m-d H:i:s');
                    } else {
                        $eventArray[$key]['start'] = (new DateTime($deviation['start']))->format('Y-m-d H:i:s');
                        $eventArray[$key]['end'] = (new DateTime($deviation['end']))->format('Y-m-d H:i:s');
                    }

                    $eventArray[$key]['original_entry_uid'] = $event['original_entry_uid'];
                    $eventArray[$key]['entry_uid'] = $deviation['uid'];
                }
            }
        }
    }

    /**
     * This function calculates, which checkboxes were checked in the backend
     *
     * @param int $checkboxValue
     * @return array
     */
    protected function getValuesFromCheckboxes(
        int $checkboxValue
    ): array {
        $activeCheckboxes = [];
        for ($i = 0; $i < 32; ++$i) {
            if ((($checkboxValue >> $i) & 1) !== 0) {
                $activeCheckboxes[] = $i + 1;
            }
        }

        return $activeCheckboxes;
    }

    /**
     * @param int $counter
     * @param int $recurrenceAmount
     * @return bool
     */
    protected function recurrenceAmountIsNotReached(
        int $counter,
        int $recurrenceAmount
    ): bool {
        if ($recurrenceAmount > 0) {
            return $counter <= $recurrenceAmount;
        }

        return true;
    }

    /**
     * @param DateTime $nextStartDateTime
     * @param DateTime $recurrenceEndDate
     * @return bool
     */
    protected function recurrenceEndDateIsNotReached(
        DateTime $nextStartDateTime,
        DateTime $recurrenceEndDate
    ): bool {
        return $recurrenceEndDate->format('U') > 0 && $nextStartDateTime <= $recurrenceEndDate;
    }

    /**
     * @param int $years
     * @throws Exception
     */
    public function setUntilDateBasedOnYears(int $years): void
    {
        $untilDate = new DateTime();
        $untilDate->add(new DateInterval('P' . $years . 'Y'));
        $this->setUntilDate($untilDate);
    }

    /**
     * @param DateTime $untilDate
     */
    public function setUntilDate(DateTime $untilDate): void
    {
        $this->untilDate = $untilDate;
    }
}
