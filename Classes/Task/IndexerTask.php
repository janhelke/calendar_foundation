<?php
declare(strict_types=1);

namespace JanHelke\CalendarFoundation\Task;

use JanHelke\CalendarFoundation\Service\EventService;
use TYPO3\CMS\Scheduler\Task\AbstractTask;

/**
 * Class IndexerTask
 */
class IndexerTask extends AbstractTask
{
    /**
     * This is the main method that is called when a task is executed
     * It MUST be implemented by all classes inheriting from this one
     * Note that there is no error handling, errors and failures are expected
     * to be handled and logged by the client implementations.
     * Should return TRUE on successful execution, FALSE on error.
     *
     * @return bool Returns TRUE on successful execution, FALSE on error
     */
    public function execute(): bool
    {
        $events = (new EventService())->createAndSaveEventIndex();
        return (bool)$events;
    }
}
