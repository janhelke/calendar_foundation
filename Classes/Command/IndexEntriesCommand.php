<?php
declare(strict_types=1);

namespace JanHelke\CalendarFoundation\Command;

use JanHelke\CalendarFoundation\Service\EventService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class IndexEntriesCommand
 */
class IndexEntriesCommand extends Command
{
    protected ?array $validatedEntryUids = null;

    /**
     * Configure the command by defining the name, options and arguments
     */
    protected function configure(): void
    {
        $this
            ->setName('calendar:index')
            ->setDescription('Indexes all calendar_framework entries.')
            ->setHelp('Indexes all entries of the table tx_calendar_entry combined with the recurrences and creates the events stored in the table tx_calendar_event.')
            ->addOption(
                'entryUids',
                'e',
                InputOption::VALUE_OPTIONAL,
                'Creates only events for choosen entries (comma separated).',
                false
            );
    }

    /**
     * Initializes the command after the input has been bound and before the input
     * is validated.
     *
     * This is mainly useful when a lot of commands extends one main command
     * where some things need to be initialized based on the input arguments and options.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        if ($input->getOption('entryUids') !== false) {
            $unvalidatedEntryUids = GeneralUtility::trimExplode(',', $input->getOption('entryUid'));
            foreach ($unvalidatedEntryUids as $unvalidatedEntryUid) {
                if ($unvalidatedEntryUid == (int)$unvalidatedEntryUid) {
                    $this->validatedEntryUids[] = $unvalidatedEntryUid;
                } else {
                    throw new \InvalidArgumentException(
                        'Invalid value entered for "entryUids". Please ensure to enter only a comma seperated list of integers.',
                        1_567_177_124
                    );
                }
            }
        } else {
            $this->validatedEntryUids = [];
        }
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            'Indexes all entries of the calendar_framework and creates events out of them.',
            '============',
        ]);

        $events = (new EventService())->createAndSaveEventIndex($this->validatedEntryUids);

        $output->writeln([
            'Done. ' . $events . ' events were created and stored in the database.',
        ]);
        return 0;
    }
}
