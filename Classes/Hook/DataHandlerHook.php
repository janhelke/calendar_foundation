<?php
declare(strict_types=1);

namespace JanHelke\CalendarFoundation\Hook;

use JanHelke\CalendarFoundation\Domain\Repository\EntryRepository;
use JanHelke\CalendarFoundation\Domain\Repository\EventRepository;
use JanHelke\CalendarFoundation\Domain\Repository\RecurrenceRepository;
use JanHelke\CalendarFoundation\Service\EventService;
use TYPO3\CMS\Core\DataHandling\DataHandler;

/**
 * Data Handler Hook
 */
class DataHandlerHook
{
    protected object $objectManager;

    protected EventRepository $eventRepository;

    protected RecurrenceRepository $recurrenceRepository;

    protected EntryRepository $entryRepository;

    /**
     * DataHandlerHook constructor.
     */
    public function __construct(
    ) {
        $this->eventRepository = new EventRepository();
        $this->entryRepository = new EntryRepository();
        $this->recurrenceRepository = new RecurrenceRepository();
    }

    /**
     * This hook runs the indexer for the newly added or updated job.
     *
     * @param string $status
     * @param string $table
     * @param string|int $id
     * @param array $fieldArray
     * @param DataHandler $pObj
     */
    public function processDatamap_afterDatabaseOperations(
        string $status,
        string $table,
        $id,
        array $fieldArray,
        DataHandler $pObj
    ): void {
        // This is not fully functional. I guess, we need a case for entries without
        // recurrences, a case for added recurrences, for changed recurrences and for
        // deleted recurrences.
        // @todo Implement the different cases.
        if ($table === 'tx_calendar_entry') {
            $eventService = new EventService();
            if (is_int($id)) {
                $this->eventRepository->removeEventsFromTableByEntryUids($id);
            } else {
                $id = $pObj->substNEWwithIDs[$id];
            }

            if (!empty($id)) {
                $entry = $this->entryRepository->findByUid($id);
                $recurrences = $this->recurrenceRepository->findByEntryUid($id) ?: [];
                if (!empty($recurrences)) {
                    foreach ($recurrences as $recurrence) {
                        $events = $eventService->calculateEventRows(
                            $entry,
                            $recurrence
                        );
                        $this->eventRepository->insertMultipleEventEntries($events);
                    }
                } else {
                    $events = $eventService->calculateEventRows(
                        $entry
                    );
                    $this->eventRepository->insertMultipleEventEntries($events);
                }
            }
        }
    }
}
